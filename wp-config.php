<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

switch(isset($_SERVER['SERVER_NAME']) ? strtolower($_SERVER['SERVER_NAME']) : 'local') {

	case 'local':
		define('DB_HOST', 'local');
		define('DB_NAME', 'isocol');
		define('DB_USER', 'local');
		define('DB_PASSWORD', 'lamb');
		define('DEV_ENVIRONMENT', true);
        define('WP_DEBUG', true);
        define('WP_DEBUG_DISPLAY', true);
        define('WP_DEBUG_LOG', true);

		break;

	case 'dev.lambagency.com.au':

		define('DB_HOST', 'localhost');
		define('DB_NAME', 'lambagen_isocol');
		define('DB_USER', 'lambagen_dbuser');
		define('DB_PASSWORD', 'cHAxaq2W');
		define('DEV_ENVIRONMENT', true);
        define('WP_DEBUG', false);
        define('WP_DEBUG_DISPLAY', false);
        define('WP_DEBUG_LOG', false);

		break;

	default:

		define('DB_HOST', 'localhost');
		define('DB_NAME', 'isocolco_wp_live');
		define('DB_USER', 'isocolco_dbuser');
		define('DB_PASSWORD', '3b,Ba%G#i}mS');
		define('DEV_ENVIRONMENT', false);
        define('WP_DEBUG', false);

		break;
}


define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K%D7|~`rqOgI,c}jNP9TMzQes;iv_< `+tJ-l>roO={}s83H0T+cO4rI&!+[I+pZ');
define('SECURE_AUTH_KEY',  'va+){WKSv|)>Ab;NQM`_B.zGv+TLme{y@$%YV<-1=Z?=J=}Px)PK$*[xHou}G1NH');
define('LOGGED_IN_KEY',    'e-lJts}i/&!.`z-o ?%E8CfvuN3a]t/+L&{H^R4RAQ,<5GMID+5>eo`{al[pE:`p');
define('NONCE_KEY',        '}L+I-H4M<Eqc#T-D1N5+o]-?rm)6.Sn/gAEx$E+g9u!DL1Suav76 4_7A6/Kwq%K');
define('AUTH_SALT',        'M[i-jlw H+by++,Bt.pfZt>(^<Y-i%[G[k=yRV-unr?|-3Cqo6E~V.S4q?cW(U1C');
define('SECURE_AUTH_SALT', 'UlU~+U?BgY37ZJD+X2STT`Zmm^<&ygL,L]GzfY!Ij,^*ES.[5B/6),rzi$Z7Go.6');
define('LOGGED_IN_SALT',   '|Q]k r=)=:H-oU@2:mt/{sK0XA~I!Y&mrM6WE<EX`+,g;]8h:Q)Y%2RV/:f@|EdQ');
define('NONCE_SALT',       '):C6l&aLC&n5W -h]6o-oDxhQiZ W4ox2Zt`1{]*Cco!SYe(v_K=f7L}>y:nDH W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'lamb_wp_';


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



define('DISALLOW_FILE_EDIT', true);
