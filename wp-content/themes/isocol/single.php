<?php
get_header();

get_layout('blog', 'single');
get_layout('blog', 'related');
get_layout('form', 'subscribe');
get_footer();