// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


// Place any jQuery/helper plugins in here.
jQuery.validator.addMethod(
    "modulus",
    function(value, element, params) {
        return this.optional(element) || !(parseFloat(value) % parseFloat(params));
    },
    "Please specify a valid number"
);


// Australian Date Validation
/**
 * Return true, if the value is a valid date, also making this formal check dd/mm/yyyy.
 *
 * @example jQuery.validator.methods.date("01/01/1900")
 * @result true
 *
 * @example jQuery.validator.methods.date("13/01/1990")
 * @result false
 *
 * @example jQuery.validator.methods.date("01.01.1900")
 * @result false
 *
 * @example <input name="pippo" class="dateAU" />
 * @desc Declares an optional input element whose value must be a valid date.
 *
 * @name jQuery.validator.methods.dateAU
 * @type Boolean
 * @cat Plugins/Validate/Methods
 */

jQuery.validator.addMethod(
    "dateAU",
    function(value, element) {
        var check = false;
        var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if( re.test(value)){
            var adata = value.split('/');
            var dd = parseInt(adata[0],10);
            var mm = parseInt(adata[1],10);
            var yyyy = parseInt(adata[2],10);
            var xdata = new Date(yyyy,mm-1,dd);
            if ( ( xdata.getFullYear() == yyyy ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == dd ) )
                check = true;
            else
                check = false;
        } else
            check = false;
        return this.optional(element) || check;
    },
    "Please enter a date in the format mm/dd/yyyy"
);


jQuery.validator.addMethod("phoneNumberAU", function(value, element) {
    if(this.optional(element)) {
        return true;
    }

    if(value.replace(/[0-9]|\(|\)|\-|\+|\s/g, '')) {
        return false;
    }

    value = value.replace(/\(|\)|\-|\+|\s/g, '');
    return value.length == 10 || value.length === 8;

}, "Please enter a valid phone number");


jQuery.validator.addMethod("ABN", function(value, element) {
    if(this.optional(element)) {
        return true;
    }


    if(value.replace(/[0-9]|\(|\)|\-|\+|\s/g, '')) {
        return false;
    }

    value = value.replace(/\(|\)|\-|\+|\s/g, '');
    return value.length == 11;

}, "Please enter a valid ABN");


jQuery.validator.addMethod("money",
    function(value, element) {
        return this.optional(element) || /^\$[0-9\,]*(\.\d{1,2})?$|^\$?[\.]([\d][\d]?)$/.test(value);
    }, "Please enter a $ amount"
);
