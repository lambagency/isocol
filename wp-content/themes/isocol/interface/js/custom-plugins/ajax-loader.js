/**
 * ======================================================================
 * AJAX LOADER
 * ======================================================================
 *
 * TODO: ajax loader documentation
 *
 * Dependencies: jquery-throttle.js
 * Authors: Carlton - LambAgency
 * ======================================================================
 *
 * Function Implementation:
 *
 *  var modBlog = $('.mod-blog');                       // THe module
 *      blogList = modBlog.find('.blog-list');          // The listing container
 *      blogUtilForm = modBlog.find('.blog-util form'); // The optional filtering form
 *
 *  blogList.ajaxLoader({
 *      action: 'blogPosts',    // The ajax action, defined in functions/ajax.php
 *      form: blogUtilForm      // optional
 *  });
 *
 * ======================================================================
 */

(function( $, window, document, undefined ){

    // Plugin constructor
    var ajaxLoader = function( element, options ){

        this.element        = element;
        this.$element       = $(element);
        this.$elementInner  = this.$element.find('.inner-container');
        this.options        = options;
        this.loading        = false; // Holds true if loading in progress
        this.searching      = false; // Holds true if search in progress

        this.$elementForm   = this.options.form;

        // Remove object from options
        delete this.options['form'];

        // Get defaults set by data attributes
        this.data = {
            postsPerPage: this.$element.data( "posts-per-page" ),
            maxPages: this.$element.data( "max-pages" ),
            page: this.$element.data( "page" ),
            nextPage: this.$element.data( "next-page" ),
            cat: this.$element.data( "cat" )
        };
    };

    // the plugin prototype
    ajaxLoader.prototype = {

        // loader defaults
        defaults: {
            page: 1,
            postsPerPage: 6
        },

        /**
         * Initialise plugin
         *
         * @returns {ajaxLoader} - Return ajax loader instance
         */
        init: function() {

            var self = this;

            // Config
            //---------------------------------

            // Config which is updated via form filtering
            this.config = $.extend({}, this.defaults, this.options, this.data);

            // Holds default config
            this.defaultConfig = $.extend({}, this.config);


            // Load More
            //---------------------------------

            this.loadMoreElement = document.createElement('DIV');

            this.loadMoreElement.className = 'load-more';
            this.loadMoreElement.innerHTML = 'Load More';
            this.loadMoreElement.setAttribute('data-loading', 'Loading');

            // Hide load more if no more posts on first load
            if (this.config.nextPage > this.config.maxPages) {
                this.loadMoreElement.className = this.loadMoreElement.className + ' hidden';
            }

            this.$elementInner[0].parentNode.insertBefore(this.loadMoreElement, this.$elementInner[0].nextSibling);

            this.loadMoreElement.addEventListener('click', function(e) {
                e.preventDefault();

                if (!self.loading) {
                    $(self.loadMoreElement).addClass('loading');

                    self.loadPosts().then(function() {
                        $(self.loadMoreElement).removeClass('loading');
                    });
                }
            });


            // Form Filtering
            //---------------------------------

            if (this.$elementForm) {

                // Form submit handler
                this.$elementForm.submit(function(e) {
                    e.preventDefault();

                    // Loop through each form input and assign name value pair to config
                    $.each($(this).find(':input'), function(i, field) {

                        var $field = $(field),
                            valueChanged = (self.config[$field.attr('name')] != $field.val());


                        // Assign new value to config
                        self.config[$field.attr('name')] = $field.val();


                        // If order field, check for orderby attribute and set orderby
                        if ($field.attr('name') == 'order') {
                            var orderby = $field.find(':selected').data('orderby');

                            self.config.orderby = orderby ? orderby : '';
                        }


                        // Check if category changed and push category href to history
                        if ($field.attr('name') == 'cat') {
                            if (valueChanged) {
                                var categoryHref = $field.find(':selected').data('href');
                                history.pushState(self.config, '', categoryHref);
                            }
                        }
                    });


                    // Action new ajax search
                    self.$elementForm.addClass('loading');
                    self.$element.addClass('loading');

                    self.newSearch().then(function() {
                        self.$elementForm.removeClass('loading');
                        self.$element.removeClass('loading');
                    });
                });


                // Submit form on select field change
                this.$elementForm.find('select').on('change', function() {
                    self.$elementForm.submit();
                });
            }


            // State Change
            //---------------------------------

            window.onpopstate = function(event) {

                if (event.state) {
                    // Update config to previous state
                    self.config = event.state;
                }
                else {
                    // Update config to default state
                    self.config = self.defaultConfig;
                }


                // Update category fields to match the category of the new state
                $.each(self.$elementForm.find(':input'), function(i, field) {

                    var $field = $(field),
                        key = $(field).attr('name');

                    // Check if field has value in config
                    if (self.config.hasOwnProperty(key)) {

                        var value = self.config[key];

                        // Check if order field
                        if (key == 'order') {

                            var selectFieldIndex = 0,
                                orderbyValue = '';

                            // If orderby is set find appropriate order option and set to active
                            if (self.config.hasOwnProperty('orderby') && self.config['orderby'] != '') {
                                orderbyValue = self.config['orderby'];
                            }

                            $field.find('option').each(function(i) {

                                var $option = $(this);

                                if ($option.val() == value) {

                                    if ($option.data('orderby') && (orderbyValue == $option.data('orderby'))) {
                                        selectFieldIndex = i;
                                    }
                                    else if (!orderbyValue && !$option.data('orderby')) {
                                        selectFieldIndex = i;
                                    }
                                }

                            });

                            $field.prop('selectedIndex', selectFieldIndex);
                        }
                        // Update field value to match config
                        else {
                            $field.val(value);
                        }
                    }
                    // Set field to default
                    else {

                        // Set select field to first option
                        if ($field.get(0).tagName == 'SELECT') {
                            $field.prop('selectedIndex', 0);
                        }
                        // Set input to empty string
                        else {
                            $field.val($field.val(''));
                        }
                    }
                });


                // Action new ajax search
                self.$elementForm.addClass('loading');
                self.$element.addClass('loading');

                self.newSearch().then(function() {
                    self.$elementForm.removeClass('loading');
                    self.$element.removeClass('loading');
                });
            };

            return this;
        },


        /**
         * Create a new search, resets the page to 1 and sets the searching status
         * to true while searching.
         *
         * @returns {*} - Returns a promise for the ajax get posts call used by loadPosts
         */
        newSearch: function() {

            var self = this;

            var defer = $.Deferred();

            self.searching = true;
            self.config.page = 1; // Set Page to 1

            self.loadPosts().then(function() {

                self.searching = false;
                defer.resolve();
            });

            return defer.promise();
        },

        /**
         * Load WP posts using the ajax get posts functions.
         * Allows filtering by category, ordering and pagination.
         *
         * @returns {*} - Returns a promise for the ajax get posts call
         */
        loadPosts: function() {

            var self = this;
            self.loading = true;

            var defer = $.Deferred();


            $.ajax({
                type: 'post',
                url: productionJS.ajaxUrl,
                dataType: 'json',
                data: {
                    'action'            : this.config.action,
                    'page'              : this.config.page,
                    'posts_per_page'    : this.config.postsPerPage,
                    'cat'               : this.config.cat,
                    'order'             : this.config.order,
                    'orderby'           : this.config.orderby
                },
                success: function(response) {

                    // Clear container if first page of posts
                    if (self.config.page == 1) {
                        self.$elementInner.html('');
                    }

                    self.$elementInner.append(response['html']); // Append response to container

                    self.config.page        = response['nextPage'];
                    self.config.maxPages    = response['maxPages'];


                    // Show load more if additional pages, hide otherwise
                    if (self.config.page > self.config.maxPages) {
                        $(self.loadMoreElement).addClass('hidden');
                    }
                    else {
                        $(self.loadMoreElement).removeClass('hidden');
                    }
                },
                error: function(errorThrown) {
                    console.log("Error: " + errorThrown);
                },
                complete: function() {
                    self.loading = false;

                    // Trigger load posts completed callback
                    self.$element.trigger('loadPosts', {});

                    defer.resolve();
                }
            });

            return defer.promise();
        }
    };

    ajaxLoader.defaults = ajaxLoader.prototype.defaults;

    $.fn.ajaxLoader = function(options) {
        return this.each(function() {
            new ajaxLoader(this, options).init();
        });
    };

})( jQuery, window , document );