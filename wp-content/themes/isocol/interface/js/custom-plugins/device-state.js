/**
 * ======================================================================
 * DEVICE STATE
 * ======================================================================
 * Create window resize listener to trigger resize events. Also triggers
 * an event when it checks a device state change, e.g. mobile to tablet,
 * by using a state holder DOM element with media queries attached.
 *
 * Also contains methods that allow the user to check the current state
 * the device (e.g. if is mobile, greater than tablet, etc)
 *
 * Dependencies: jquery-throttle.js
 * Authors: Carlton - LambAgency
 * ======================================================================
 *
 * Function Implementation:
 *
 * $.device('state')            // Returns device state (returns string e.g. 'mobile')
 * $.device('min', 'mobile')    // Returns device state is greater than that specified (returns true|false)
 * $.device('max', 'mobile')    // Returns device state is less than or equal to that specified (returns true|false)
 * $.device('between', 'mobile', 'desktop) // Returns device state is between that specified (returns true|false)
 *
 *
 * State Change Listener:
 * Fires when the state changes (e.g. mobile to tablet-portrait, desktop to tablet-landscape)
 *
 * device.on('stateChange', function() {
 *      // Do Stuff
 * }
 *
 *
 * Resize Listener:
 * Fires when the window is resized. Utilises jquery throttling to improve performance
 *
 * device.on('resized', function() {
 *      // Do Stuff
 * }
 *
 * ======================================================================
 */

;(function ( $, window, document, undefined ) {

    var $body   = $('body'),
        $stateIndicator = $('.state-indicator'),
        stateMap = {
            0: '',
            1: 'mobile',
            2: 'tablet-portrait',
            3: 'tablet-landscape',
            4: 'desktop'
        },
        state   = 0; // Holds the device state key

    // Private Methods
    var methods = {

        /**
         * Initialise the device resize/state listeners
         * @param options - Array of options
         */
        init: function(options) {
            publicMethods.state(); // Initial state check

            methods.resizeListener(); // Init resize listener
        },

        /**
         * Listen to window resize event and fire resize function on resize
         */
        resizeListener: function() {

            // Timeout used to ensure $.throttle is loaded
            setTimeout(function() {
                if (window.addEventListener){
                    window.addEventListener("resize", $.throttle(publicMethods.resize, 250, 0, true)); // Throttle resize
                } else if (window.attachEvent){
                    window.attachEvent("resize", $.throttle(publicMethods.resize, 250, 0, true)); // Throttle resize
                }
            });
        },

        /**
         * Reverse lookup the state map object and get the state key by
         * value if it exists.
         *
         * @param value - State string name
         * @returns {*} - Returns the state key
         */
        getStateKeyByValue: function(value) {
            for( var state in stateMap ) {
                if(stateMap.hasOwnProperty(state)) {
                    if(stateMap[ state ] === value)
                        return state;
                }
            }

            return -1;
        }

    };

    // Public Methods
    var publicMethods = {

        /**
         * Resize event, called when window is resized
         */
        resize: function() {
            $body.trigger('resized', {state: state}); // Trigger Resize

            var lastState = state; // Store last state
            publicMethods.state(); // Update state

            if (state != '' && (lastState != state)) {
                publicMethods.stateChange(); // State Change
            }
        },

        /**
         * State change event, called when the screen is resized
         * and changes state. (e.g. mobile to tablet)
         */
        stateChange: function() {
            $body.trigger('stateChange', {state: state}); // Trigger State Change
        },

        /**
         * Get the state of the window, using a state indicator DOM element.
         * The DOM element is defined in the css and uses the z-index as the key
         * element to define the device state with the 'content' field.
         *
         * @param options   - (String) The state name to check ('mobile', etc)
         *                  - (Object) The min and/or max value to check ({min: 'mobile', max: 'desktop'})
         *
         * @returns {*}     - If options is not set, returns the current state name ('mobile', etc)
         *                  - If options is string, returns true if the requested state name matches the current state
         *                  - If options is object, returns the result of the appropriate method (between, min, max)
         */
        state: function(options) {

            if ($stateIndicator.length) {
                var stateKey = $stateIndicator.css('z-index'); // Use element z-index as key

                if( stateMap[stateKey] !== undefined ) {
                    state = stateKey; // Update state key
                }
            }

            if (options) {

                // Options is string
                if (typeof options == 'string') {
                    return (options == stateMap[state]);
                }
                // Options is object
                else if (typeof options == 'object') {

                    var minState = (options['min'] !== undefined) ? options['min'] : '',
                        maxState = (options['max'] !== undefined) ? options['max'] : '';

                    if (minState && maxState) {
                        return publicMethods.between(minState, maxState);
                    }
                    else if (minState) {
                        return publicMethods.min(minState);
                    }
                    else if (maxState) {
                        return publicMethods.max(maxState);
                    }
                }
            }
            // Default to return state name if no options set
            else {

                return stateMap[state];
            }

            return false; // Return false if invalid options set
        },

        /**
         * Check if the current state is greater than the minimum requested.
         *
         * @param minState      - The name of the minimum state ('mobile', etc)
         *
         * @returns {boolean}   - Returns true if the current state is greater than the minimum state
         */
        min: function(minState) {

            if (typeof minState !== 'string') {
                return false;
            }

            var minStateKey = methods.getStateKeyByValue(minState); // Reverse lookup min state by key

            if (minStateKey >= 0) {
                return state > minStateKey; // Current state > Minimum State
            }
            else {
                return false;
            }
        },

        /**
         * Check if the current state is less than or equal to the maximum requested.
         *
         * @param maxState      - The name of the maximum state ('mobile', etc)
         *
         * @returns {boolean}   - Returns true if the current state is less than or equal to the maximum state
         */
        max: function(maxState) {

            if (typeof maxState !== 'string') {
                return false;
            }

            var maxStateKey = methods.getStateKeyByValue(maxState); // Reverse lookup mxa state by key

            if (maxStateKey >= 0) {
                return state <= maxStateKey; // Current state <= Maximum State
            }
            else {
                return false;
            }
        },

        /**
         * Checks if the current state is greater than the minimum and less than the maximum.
         *
         * @param minState      - The name of the minimum state ('mobile', etc)
         * @param maxState      - The name of the maximum state ('mobile', etc)
         *
         * @returns {boolean}   - Returns true if current state is between the minimum and maximum
         */
        between: function(minState, maxState) {

            if (typeof minState !== 'string' && typeof maxState !== 'string') {
                return false;
            }

            var minStateKey = methods.getStateKeyByValue(minState), // Reverse lookup min state by key
                maxStateKey = methods.getStateKeyByValue(maxState); // Reverse lookup max state by key

            if (minStateKey >= 0 && maxStateKey >= 0) {

                // Current state > Minimum State && Current state <= Maximum State
                return (state > minStateKey) && (state < maxStateKey);
            }
            else {
                return false;
            }
        }
    };


    $.device = function ( methodOrOptions ) {

        var defaults = {};

        // If method is called
        if (publicMethods[methodOrOptions]) {

            return publicMethods[methodOrOptions].apply(this, Array.prototype.slice.call( arguments, 1 ));

            // Default to animate element
        } else if (typeof methodOrOptions === 'object' || ! methodOrOptions ) {

            var options = $.extend( {}, defaults, methodOrOptions);

            methods.init.apply(this, arguments); // Animate element

            return $body;

            // If method called does not exist
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on device' );
        }
    };

    window.device = $.device(); // Initialise global instance

})( jQuery, window, document );