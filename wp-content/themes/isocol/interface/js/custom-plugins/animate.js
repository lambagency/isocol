/**
 * =======================================================
 * ANIMATE
 * =======================================================
 * Element show and hide animations
 *
 * Authors: Carlton - LambAgency
 * =======================================================
 *
 * Plugin can be called on single or multiple elements.
 * Allows for both entrance and hiding animations.
 *
 * Uses Data Attributes for element specific animation options:
 * data-animate : - This will be the animation CSS class that will be added
 * data-animate-delay : - The animation delay in milliseconds
 * data-animate-duration : - The duration of the animation, ensure delay class is generated in css (d200)
 *
 *
 * Animate Element Example:
 * var elements = $('.sample-elements');
 * elements.customAnimate({delay: 400});
 *
 *
 * Hide Element Example:
 * var elements = $('.sample-elements');
 * elements.customAnimate('hide');

 * Example Element:
 * <div class="sample element" data-animate="fadeIn" data-duration="1000"></div>
 *
 * ======================================================================
 */
;(function ( $, window, document, undefined ) {

    var methods = {

        /**
         * Animate an element by adding a css animation class.
         * Allows for animation duration, effect and delay options.
         * Uses the element data attribute for options overrides.
         *
         * @param options - Animation options.
         */
        animate: function(options) {

            var element = $(this);

            var dataOptions = {
                animation:  element.data('animate'),
                delay:      element.data('animate-delay'),
                duration:   element.data('animate-duration')
            };

            var $options = $.extend({}, options, dataOptions);

            setTimeout(function() {
                if ($options.duration !== undefined) {
                    element.addClass('d' + $options.duration);
                }

                element.addClass($options.animation).css('visibility', 'visible');
            }, $options.delay);
        },

        /**
         * Hide the animated element, removes any animation classes added by the animate function.
         * Sets the element visibility to hidden.
         *
         * @param options - Not used
         */
        hide: function(options) {

            var element     = $(this),
                animation   = element.data('animate'),
                duration    = element.data('animate-duration');

            element.removeClass(animation);
            element.removeClass('d' + duration);
            element.css('visibility', '');
        }
    };


    $.fn.customAnimate = function ( methodOrOptions ) {

        var defaults = {
            animation:  undefined, // CSS Animation name (string)
            duration:   undefined, // Animation duration (ms), ensure delay class is generated in css (d200)
            delay:      0          // Animation Delay (ms)
        };

        // If method is called
        if (methods[methodOrOptions]) {

            this.each(function() {
                return methods[methodOrOptions].apply(this, Array.prototype.slice.call( arguments, 1 ));
            });

        // Default to animate element
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {

            var options     = $.extend( {}, defaults, methodOrOptions),
                $arguments  = arguments;

            this.each(function() {
                return methods.animate.apply(this, $arguments); // Animate element
            });
        // If method called does not exist
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on customAnimate' );
        }
    };


})( jQuery, window, document );