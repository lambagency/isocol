/**
 * ======================================================================
 * LAZY IMAGE LOADER
 * ======================================================================
 * Global lazy image load desktop/mobile images from data attributes
 *
 * Dependencies: bLazy https://github.com/dinbror/blazy/
 * Authors: Carlton - LambAgency
 * ======================================================================
 *
 * Plugin must be initialised with lazyImages.init();
 *
 * The plugin will look for all occurrences of elements with the "lazy-image" class
 * Will check for mobile image if screen width is less than the mobile breakpoint, otherwise will default to desktop image
 * Mobile image is set with "data-mobile-src", desktop image is set with data-src
 *
 * Additional data attributes can be added to enable additional functionality:
 * data-lazy-parent : - data attribute will add success class to parent container
 *
 * Example Element:
 * <div class="lazy-image" data-src="http://example.com/desktop-image.jpg" data-mobile-src="http://example.com/mobile-image.jpg" data-lazy-parent></div>
 *
 * ======================================================================
 */
(function($) {

    window.lazyImages = {

        /**
         * Initialise the global lazy load object
         * @param options - Optional parameters to override or extend the bLazy plugin params
         */
        init: function(options) {

            lazyImages.settings = $.extend(true, {
                selector: '.lazy-image',
                successClass: 'lazy-success',
                breakpoints: [{
                    width: 1024, // max-width
                    src: 'data-src-mobile'
                }],
                success: function(element) {
                    var lazyElement = $(element);


                    /**
                     * Check for additional data attributes
                     */

                    // data-lazy-parent
                    if (lazyElement.attr('data-lazy-parent') !== undefined) {
                        lazyElement.parent().addClass('lazy-parent-success');
                    }
                }
            }, options);

            window.lazyImageHolder = new Blazy(
                lazyImages.settings
            );
        },

        /**
         * Refresh the global lazy loader. Useful if images have been added or removed.
         * Recommended to call on slider initialisation
         */
        refresh: function() {
            lazyImageHolder.revalidate();
        }
    };

})(jQuery);