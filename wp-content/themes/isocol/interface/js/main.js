jQuery(document).ready(function() {

    _main.initBase();
    _main.initModules();
    _main.wpadminbar();
    _main.initModernizrFunctions();
    _main.initAnimations();
    _main.initLazyImages();
    _main.initCheckHashInURL();
    _main.initScrollTo();
});


var _main = _main ? _main : {

    initBase: function() {

        var header = $('header'),
            footer = $('footer');

        // Init Header
        header.modheader();

        // Init Menu
        header.modmenu();

        // Init Header
        footer.modfooter();
    },

    initModules: function() {

        // Loop through all modules and initialise
        $('.module').each(function() {
            var moduleID        = $(this).data('module');

            // Check if module has module ID defined (data-module="")
            if (typeof moduleID !== 'undefined') {

                var moduleFuncName  = 'mod' + moduleID;

                // Check if function for module exists
                if ($.isFunction($.fn[moduleFuncName])) {
                    $(this)[moduleFuncName]();
                }
            }
        });
    },

    wpadminbar: function()
    {
        if($('#wpadminbar').length) {
            $('header').addClass('wpadminbar');
        }
    },


    initModernizrFunctions: function()
    {
        // add placeholder for IE9 and below
        if (typeof Modernizr == 'object') {
            if (Modernizr && !Modernizr.input.placeholder) {
                $('[placeholder]').focus(function () {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                        input.removeClass('placeholder');
                    }
                }).blur(function () {
                    var input = $(this);
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.addClass('placeholder');
                        input.val(input.attr('placeholder'));
                    }
                }).blur();

                $('[placeholder]').parents('form').submit(function () {
                    $(this).find('[placeholder]').each(function () {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                            input.val('');
                        }
                    })
                });
            }
        }
    },

    initAnimations: function() {

        if (themeConfig.isDesktop()) {

            var animatedElements = $('.animated').not('[data-animate-hold]');

            animatedElements.appear({
                force_process: true // Force on load
            });

            // Animate on element appear
            animatedElements.on('appear', function() {
                $(this).customAnimate();
            });

            // Animate on element disappear
            animatedElements.on('disappear', function() {
                if (typeof $(this).data('animate-repeat') !== 'undefined') {
                    $(this).customAnimate('hide');
                }
            });

            // Force appear to run if device state change
            device.on('stateChange', function() {
                $.force_appear();
            });
        }
        else {
            $('.animated').css('visibility', 'visible');
        }
    },

    initLazyImages: function() {

        lazyImages.init();

        // Force lazy images refresh if device state change
        device.on('stateChange', function() {
            lazyImages.refresh();
        });
    },

    initCheckHashInURL: function()
    {
        if(window.location.hash) {
            var hash    = window.location.hash.substring(1);
            var target  = $('#' + hash);

            if (target.length) {

                var scrollOffset = _main.initScrollOffset();

                scrollTo(target, 400, scrollOffset);
            }
        }
    },


    initScrollTo: function()
    {

        $('a.scroll-to, .scroll-to > a').click(function(e) {
            e.preventDefault();

            var ele = $(this);

            // check for scroll position data attribute
            var scrollToEle = ele.data('scroll-to') !== undefined ? ele.data('scroll-to') : false;
            scrollToEle     = scrollToEle ? (scrollToEle == 'module' ? $('.' + scrollToEle + ':nth-of-type(2)') : $('#' + scrollToEle)) : false;

            // Target
            var target      = scrollToEle ? scrollToEle : this.hash;

            // Duration
            var duration = $(this).data('scroll-duration');

            _main.scrollTo(target, duration);
        });
    },

    scrollTo: function(element, duration) {
        // default scroll animation time (ms)
        // Use data-scroll-duration="" to override
        var scrollDurationDefault = 800;

        duration = (duration === undefined) ? scrollDurationDefault : duration;

        var scrollOffset = _main.initScrollOffset();

        scrollTo(element, duration, scrollOffset);
    },

    initScrollOffset: function()
    {
        var wpadminbar      = $('#wpadminbar').height();
        var header          = $('header'); // header - used to define offset height
        var headerFixed     = header.css('position') == 'fixed';

        // Offset
        var scrollOffset    = 0;

        scrollOffset += wpadminbar;

        if (headerFixed) {
            scrollOffset += header.height();
        }

        return scrollOffset;
    }

};