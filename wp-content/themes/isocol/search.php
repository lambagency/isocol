<?php

/*
Template Name: Search Page
*/

get_header();
get_layout('banner');
get_layout('search-results');
get_footer();