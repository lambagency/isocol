"use strict";

(function( $ ){

    var moduleID = 'home', // Must equal (data-module)
        module;                 // Holds module element

    var moduleFunctions = {

        init: function() {

        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );