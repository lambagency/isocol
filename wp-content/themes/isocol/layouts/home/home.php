<?php

$additionalFields = array(
    'heroImage' => 'hero_image',

    'productFeature'  => 'product_feature',
    'productFeature1' => 'product_feature_1',
    'productFeature2' => 'product_feature_2',
    'productFeature3' => 'product_feature_3',
    'productFeature4' => 'product_feature_4',
    'productFeature5' => 'product_feature_5'

);

$fields = LambAgency\Util\getFields($additionalFields);

?>

<div data-module="home" class="module mod-home <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

<!--        <div class="title-block-mobile">-->
<!--            --><?php //include(locate_template('layouts/components/title-block.php')); ?>
<!--            <a href="#" role="button" class="scroll-to animated" data-scroll-to="module"><i class="fa fa-angle-down fa-3x"></i></a>-->
<!---->
<!--        </div>-->


        <div class="hero-image animated" <?php addAnimation('fadeIn'); ?>>

            <?php if ($fields['heroImage']) : ?>
                <div class="image" style="background-image: url(<?php echo $fields['heroImage']['sizes']['module-bg']; ?>)"></div>
            <?php endif; ?>


            <div class="product-features">

                    <div class="feature feature-1 animated" <?php addAnimation('fadeIn', 800); ?>>
                        <div class="item">
                            <?php if ($fields['productFeature']) : ?>
                                <p> <?php echo $fields['productFeature']; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="border border-1"></div>
                    </div>

                    <div class="circles animated" <?php addAnimation('fadeIn', 3000); ?>>
                        <div class="circle circle-1"></div>
                        <div class="circle circle-2"></div>
                        <div class="circle circle-3"></div>
                        <div class="circle circle-4"></div>
                        <div class="circle circle-5"></div>
                        <div class="circle circle-6"></div>
                        <div class="circle circle-7"></div>
                        <div class="circle circle-8"></div>
                    </div>

                    <div class="feature feature-2 animated" <?php addAnimation('fadeIn', 1000); ?>>
                        <div class="item">
                            <?php if ($fields['productFeature1']) : ?>
                               <p><?php echo $fields['productFeature1']; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="border border-2"></div>
                    </div>


                    <div class="feature feature-3 animated" <?php addAnimation('fadeIn', 1400); ?>>
                        <div class="item">
                            <?php if ($fields['productFeature2']) : ?>
                                <p><?php echo $fields['productFeature2']; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="border border-3"></div>
                    </div>


                    <div class="feature feature-4 animated" <?php addAnimation('fadeIn', 1800); ?>>
                        <div class="item">
                            <?php if ($fields['productFeature3']) : ?>
                                <p><?php echo $fields['productFeature3']; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="border border-4"></div>
                    </div>


                    <div class="feature feature-5 animated" <?php addAnimation('fadeIn', 2200); ?>>
                        <div class="item">
                            <?php if ($fields['productFeature4']) : ?>
                                <p><?php echo $fields['productFeature4']; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="border border-5"></div>
                    </div>


                    <div class="feature feature-6 animated" <?php addAnimation('fadeIn', 2600); ?>>
                        <div class="item">
                            <?php if ($fields['productFeature5']) : ?>
                                <p><?php echo $fields['productFeature5']; ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="border border-6"></div>
                    </div>

            </div>


            <?php include(locate_template('layouts/components/title-block.php')); ?>

            <a href="#" role="button" class="scroll-to animated" data-scroll-to="module"><i class="fa fa-angle-down fa-3x"></i></a>

        </div>


        <?php include(locate_template('layouts/components/content.php')); ?>

        <?php include(locate_template('layouts/components/buttons.php')); ?>

    </div>

</div>