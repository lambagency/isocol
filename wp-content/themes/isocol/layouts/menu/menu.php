<?php
/*
 * Add any of the following classes to specify the type of menu:
 *
 */
?>

<div class="menu-wrap menu">

	<a class="mobile-menu-toggle"><span></span></a>

	<nav role="navigation">
        <div class="menu-container">
            <?php
            if (has_nav_menu('header')) :
                wp_nav_menu(array(
                    'walker'            => new Custom_Walker_Nav_Menu,
                    'theme_location'    => 'header',
                    'menu_class'        => 'menu menu-main',
                    'container'         => ''
                ));
            endif;
            ?>


            <div class="menu-top-wrap">

                <div class="menu-container">
                    <?php
                    if (has_nav_menu('top')) :
                        wp_nav_menu(array(
                            'walker'            => new Custom_Walker_Nav_Menu,
                            'theme_location'    => 'top',
                            'menu_class'        => 'menu menu-top',
                            'container'         => ''
                        ));
                    endif;
                    ?>
                </div>

            </div>


        </div>
	</nav>

</div>