"use strict";

(function( $ ){

    var moduleID = 'menu',  // Must equal (data-module)
        module;             // Holds module element

    var body        = $('body'),
        navigation  = $('.menu-wrap nav');


    var moduleFunctions = {

        init: function() {
            this.initMobileMenu();
            this.initSubMenu();
        },

        initMobileMenu: function () {

            var animationTime   = 400,
                animationTimer,
                menuToggle      = $('.mobile-menu-toggle');

            menuToggle.click(function (e) {
                e.preventDefault();

                $(this).toggleClass('active');
                body.toggleClass('menu-active');
                navigation.toggleClass('is-opened');
                navigation.addClass('animating');

                clearTimeout(animationTimer);

                animationTimer = setTimeout(function() {
                    navigation.removeClass('animating');
                }, animationTime);

            });
        },

        initSubMenu: function () {
            var subMenuToggles = navigation.find('.sub-menu-toggle');

            subMenuToggles.click(function() {

                $(this).parent().toggleClass('open');
            });
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );