<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9 ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<?php if (defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) : ?>
		<meta name="robots" content="noindex, nofollow"/>
    <?php else : ?>
        <meta name="robots" content="index, follow">
	<?php endif; ?>

	<?php gravity_form_enqueue_scripts(1, true); ?>

	<?php wp_head(); ?>

</head>

<body <?php body_class(''); ?>>

<div id="top" class="layout header-fixed">

    <header>

        <div class="layoutwidth header-bottom layoutwidth">
            <?php if ($headerLogo = get_field('header_logo', 'option')) : ?>
                <div class="header-logo animated"  <?php addAnimation('fadeIn'); ?>>
                    <a href="<?php echo BASE_URL; ?>" title="<?php bloginfo('name'); ?>">
                        <div class="logo" style="background-image: url(<?php echo $headerLogo['sizes']['header-logo']; ?>)"></div>
                    </a>
                </div>
            <?php endif; ?>

            <?//php include(locate_template('layouts/components/search-form.php')); ?>

            <?php get_layout('menu'); ?>
            <?php get_layout('menu', 'top'); ?>

        </div>

        <?php
        /*
        if ($phoneNumber = get_field('phone', 'option')) : ?>
            <a class="phone-number" href="tel: <?php echo $phoneNumber; ?>">
                <span class="phone-icon fa fa-phone"></span>
                <span class="phone-pre-text">Talk to us</span>
                <span class="phone-text"><?php echo $phoneNumber; ?></span>
            </a>
        <?php endif;
        */
        ?>

    </header>