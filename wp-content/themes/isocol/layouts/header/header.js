"use strict";

(function( $ ){

    var moduleID = 'header',    // Must equal (data-module)
        module;                 // Holds module element


    var moduleFunctions = {

        init: function() {

            var header        = $('header'),
                scrollTopBase = 0,
                scrollTop     = 0,
                minScroll     = 50;

            $(window).scroll(function () {

                var scrollTopNew = $(this).scrollTop();

                if (scrollTopNew > minScroll) {
                    header.addClass('compact');
                }
                else {
                    header.removeClass('compact');
                }
            });
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );