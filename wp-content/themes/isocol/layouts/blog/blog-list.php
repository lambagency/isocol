<?php

// Number of posts to load per call
$postsPerPage = get_option('posts_per_page');

$postArgs = array(
    'posts_per_page' => $postsPerPage
);


// Get category ID if category page
global $wp_query;

$categoryID = is_category() ? ($wp_query->get_queried_object_id()) : false;

if ($categoryID) {
    $postArgs['cat'] = $categoryID;
}


// Get Posts
$posts  = \Lambagency\Ajax\blogPosts($postArgs, array(), true);

$postsHTML       = $posts['html'];        // HTML of first page of posts
$postsMaxPages   = $posts['maxPages'];    // The max pages to load
$postsNextPage   = $posts['nextPage'];    // The next posts page to load

$postData = $postArgs['posts_per_page'] ? 'data-posts-per-page="' . $postArgs['posts_per_page'] . '"' : '';
$postData .= ' data-max-pages="' . $postsMaxPages . '"';
$postData .= ' data-page="' . $postsNextPage . '"';
$postData .= ' data-next-page="' . $postsNextPage . '"';
$postData .= $categoryID ? ' data-cat="' . $categoryID . '"' : '';
?>


<div data-module="bloglist" class="module mod-blog mod-blog-list">

    <?php include(locate_template('layouts/blog/blog-util.php')); ?>

    <div class="layoutwidth">
        <?php include(locate_template( 'layouts/components/title-block.php' )); ?>
    </div>

    <div class="blog-list" <?php echo $postData; ?>>
        <div class="layoutwidth">

            <div class="inner-container row grid-no-side-gutter">
                <?php if($posts) : ?>
                    <?php echo $postsHTML; ?>
                <?php endif; ?>
            </div>

        </div>
    </div>

</div>