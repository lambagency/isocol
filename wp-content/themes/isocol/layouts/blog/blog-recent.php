<?php

$fields =  LambAgency\Util\getFields();


// Posts
//--------------------------

$postArgs = array(
    'posts_per_page' => 8
);

$posts  = \Lambagency\Ajax\blogPosts($postArgs, array(), true);


// Buttons
//--------------------------

$fields['buttons'] = array(

    'container' => false,
    'buttons' => array(
        array(
            'text'                  => 'Visit the Blog',
            'button_type'           => 'link',
            'link'                  => get_permalink(get_option('page_for_posts')),
            'button_colour'         => 'transparent',
            'button_icon_position'  => 'right',
            'button_icon'           => 'fa-arrow-right'
        )
    )
);

?>


<div class="module mod-blog mod-blog-recent">

    <div class="layoutwidth">
        <?php include(locate_template( 'layouts/components/title-block.php' )); ?>
    </div>

    <div class="blog-list">
        <div class="layoutwidth">

            <div class="inner-container row grid-no-side-gutter">
                <?php if($posts) : ?>
                    <?php echo $posts['html']; ?>
                <?php endif; ?>
            </div>

            <div class="text-right">
                <?php include(locate_template( 'layouts/components/buttons.php' )); ?>
            </div>
        </div>
    </div>

</div>