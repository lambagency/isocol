<?php
$args = array(
    'type'          => 'post',
    'hide_empty'    => 1
);

$postCategories = get_categories($args);

$categoryID = is_category() ? ($wp_query->get_queried_object_id()) : false; // Get category ID if category page

?>

<div class="blog-util">
    <form>

        <div class="select-container">
            <select name="cat">
                <option value="" data-href="<?php echo get_permalink( get_option('page_for_posts' ) ); ?>">All Categories</option>
                <?php foreach ($postCategories as $postCategory) : ?>
                    <option value="<?php echo $postCategory->term_taxonomy_id; ?>" data-href="<?php echo get_category_link( $postCategory->term_taxonomy_id ); ?>" <?php if ($categoryID == $postCategory->term_taxonomy_id) { echo 'selected'; } ?>>
                        <?php echo $postCategory->name; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="select-container">
            <select name="order">
                <option value="DESC">Newest First</option>
                <option value="ASC">Oldest First</option>
                <option value='ASC' data-orderby="title">Post Title</option>
            </select>
        </div>

    </form>
</div>