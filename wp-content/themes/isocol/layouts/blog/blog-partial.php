<?php

// Categories
//------------------------------

$category = array();
$categories = get_the_terms(get_the_ID(), 'category');

if ($categories) {
    $firstCategory = array_pop($categories);

    $category['name'] = $firstCategory->name;
}


// Post Image
//------------------------------

$blogPostImageURL = false;
$useFeatureImage = get_field('feature_image');

if (isset($useFeatureImage) && $useFeatureImage) {
    $blogPostImageURL = wp_get_attachment_image_src(get_post_thumbnail_id(), 'square-medium')[0];
} else {
    $blogPostImage = get_field('banner_image');
    $blogPostImageURL = $blogPostImage ? $blogPostImage['sizes']['square-medium'] : false;
}
?>


<div class="blog-post col col-m-1-2 col-1-3">

    <div class="inner">

        <div class="card-hover">
            <div class="hover-inner">
                <span class="date"><?php echo get_the_date('F d, Y'); ?></span>
                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                <span class="excerpt"><?php $content = get_the_content(); echo wp_trim_words( $content , '20' ); ?></span>
                <a href="<?php the_permalink(); ?>" class="hover-link">Read More</a>
            </div>
        </div>

        <div class="post-top">

            <?php if ($blogPostImageURL) : ?>
                <div class="post-image lazy-image" data-src="<?php echo $blogPostImageURL; ?>"></div>
            <?php endif; ?>

            <a href="<?php the_permalink(); ?>" class="link"></a>
        </div>

        <div class="post-bottom">
            <span class="date"><?php echo get_the_date('F d, Y'); ?></span>
            <h4><?php the_title(); ?></h4>
        </div>

    </div>
</div>
