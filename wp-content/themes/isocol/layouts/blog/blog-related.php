<?php

$additionalOptionsFields = array(
    'subtitle'  => 'blog_related_posts_subtitle',
    'title'     => 'blog_related_posts_title'

);

$fields =  LambAgency\Util\getFields(array(), $additionalOptionsFields);


// Posts
//--------------------------


$postArgs = array(
    'posts_per_page' => 4
);

// Exclude self from related
$postArgs['post__not_in'] = array($post->ID);

// Get Post categories
$relatedCategories = wp_get_post_categories($post->ID);

if (is_array($relatedCategories) && count($relatedCategories) > 0) {
    $postArgs['category__in'] = $relatedCategories;
}

$posts  = \Lambagency\Ajax\blogPosts($postArgs, array(), true);

?>

<?php if ($posts) : ?>
    <div class="module mod-blog mod-blog-related">

        <div class="layoutwidth">
            <?php include(locate_template( 'layouts/components/title-block.php' )); ?>
        </div>

        <div class="blog-list">
            <div class="layoutwidth">

                <div class="inner-container row grid-no-side-gutter">
                    <?php if($posts) : ?>
                        <?php echo $posts['html']; ?>
                    <?php endif; ?>
                </div>

            </div>
        </div>

    </div>
<?php endif; ?>
