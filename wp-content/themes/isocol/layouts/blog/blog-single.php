<?php

$title      = get_the_title();
$postDate   = get_the_date('F j, Y');
$postAuthor = get_the_author();
$useFeature = get_field('feature_image');

if ($useFeature) {
    $bgImageURL = wp_get_attachment_image_src(get_post_thumbnail_id(), 'banner-bg')[0];
    $bgImageMobileURL = wp_get_attachment_image_src(get_post_thumbnail_id(), 'banner-bg-mobile')[0];
}
else {
    $bannerImg  = get_field('banner_image');
    $bgImageURL = $bannerImg ? $bannerImg['sizes']['banner-bg'] : false;
    $bgImageMobileURL = $bannerImg ? $bannerImg['sizes']['banner-bg-mobile'] : false;
}


?>

<div class="module mod-blog-banner">

    <div class="layoutwidth">
        <div class="blog-title">
            <div class="post-date animated"<?php addAnimation('fadeInDown'); ?>><span class="">Posted On </span><?php echo $postDate; ?></div>
            <h1 class="animated"<?php addAnimation('fadeIn', 600); ?>><?php echo $title; ?></h1>
            <?php the_category(); ?>
        </div>

        <div class="banner">
            <div class="banner-bg lazy-image" data-lazy-parent data-src="<?php echo $bgImageURL; ?>" data-src-mobile="<?php echo $bgImageMobileURL; ?>"></div>
        </div>
    </div>
</div>


<div class="module mod-blog mod-blog-single">

	<div class="layoutwidth">

        <?php include(locate_template( 'layouts/breadcrumbs/breadcrumbs.php' )); ?>

        <div class="blog-content">
            <?php the_content(); ?>
        </div>

        <div class="post-share">

            <?php if (class_exists('EasySocialShareButtons3')) : ?>
                <span class="share-title">Share this Post</span>
                <?php echo do_shortcode('[easy-social-share buttons="facebook,google,pinterest" counters=0 style="button"]'); ?>
            <?php endif; ?>
        </div>

	</div>
</div>