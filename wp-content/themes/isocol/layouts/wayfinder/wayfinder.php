<?php
$fields = LambAgency\Util\getFields();
$wayfinderType = $fields['type'];

if ($wayfinderType == 'links') {
    $fields['wayfinder'] = get_sub_field('wayfinder_items');
}
elseif ($wayfinderType == 'child') {
    $fields['wayfinder'] = get_children(array(
        'post_type'         => 'page',
        'post_status'       => 'publish',
        'posts_per_page'    => -1,
        'post_parent'       => get_the_ID(),
        'orderby'           => 'ID',
        'order'             => 'ASC'
    ));
}

?>

<div data-module="wayfinder" class="module mod-wayfinder <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

        <?php include(locate_template('layouts/components/title-block.php')); ?>

        <?php include(locate_template('layouts/components/content.php')); ?>


        <?php if (isset($fields['wayfinder']) && $fields['wayfinder']) : ?>
            <div class="wayfinder row grid-no-side-gutter">

                <?php $i = 1; ?>

                <?php foreach ($fields['wayfinder'] as $item) : ?>

                    <?php

                    if ($wayfinderType == 'links') :
                        $imgUrl     = isset($item['wayfinder_image']) && $item['wayfinder_image'] ? $item['wayfinder_image']['sizes']['banner-bg-mobile'] : false;
                        $title      = isset($item['title']) && $item['title'] ? $item['title'] : false;
                        $content    = isset($item['content']) && $item['content'] ? $item['content'] : false;
                        $link       = isset($item['link']) && $item['link'] ? $item['link'] : '';
                    elseif ($wayfinderType == 'child') :
                        $img        = get_field('wayfinder_image', $item->ID);
                        $imgUrl     = $img ? $img['sizes']['banner-bg-mobile'] : false;
                        $title      = $item->post_title;
                        $content    = get_field('wayfinder_summary', $item->ID);
                        $link       = get_the_permalink($item->ID);
                    endif;

                    ?>

                    <div class="col col-m-1-2 col-l-1-2 col-1-4">
                        <div class="wayfinder-item animated"  data-animate="fadeIn" data-animate-duration="400" data-animate-delay="<?php echo $i * 400; ?>">

                            <?php if ($imgUrl) : ?>

                                <div class="img" style="background-image: url(<?php echo $imgUrl; ?>);">

                                    <?php if (in_array($fields['styleType'], array('title-overlay', 'title-overlay-summary-hover'))) : ?>

                                        <h3><?php echo $title; ?></h3>

                                        <?php if ($fields['styleType'] == 'title-overlay-summary-hover') : ?>

                                            <div class="card-hover">
                                                <div class="inner">
                                                    <strong class="title"><?php echo $title; ?></strong>

                                                    <?php if (isset($content) && $content) : ?>
                                                        <div class="content">
                                                            <?php echo $content; ?>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="read-more">Read More <i class="fa fa-angle-right"></i></div>
                                                </div>
                                            </div>

                                        <?php endif; ?>

                                        <a href="<?php echo $link; ?>" class="link"></a>

                                    <?php endif; ?>

                                </div>
                            <?php endif; ?>


                            <?php if ($fields['styleType'] == 'detail-below') : ?>
                                <div class="detail" data-mh="wayfinder-detail">

                                    <h3><?php echo $title; ?></h3>

                                    <?php if (isset($content) && $content) : ?>
                                        <div class="content">
                                            <?php echo $content; ?>
                                        </div>
                                    <?php endif; ?>

                                    <a href="<?php echo $link; ?>" class="link"></a>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>

                    <?php $i++; ?>

                <?php endforeach; ?>
            </div>
        <?php endif; ?>


        <?php include(locate_template('layouts/components/buttons.php')); ?>

    </div>

</div>