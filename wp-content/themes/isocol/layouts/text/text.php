<?php

$additionalFields = array(
    'hideMobile' => 'hide_mobile'
);

$fields =  LambAgency\Util\getFields($additionalFields);


// Module Class
//--------------------------

$fields['content'] ? false : ($fields['moduleClass'][] = 'double-padding');

?>

<div data-module="text" class="module mod-text <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

        <?php include(locate_template('layouts/components/title-block.php')); ?>

        <?php include(locate_template('layouts/components/content.php')); ?>

        <?php include(locate_template('layouts/components/buttons.php')); ?>

    </div>

</div>