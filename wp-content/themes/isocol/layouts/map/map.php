<?php if ($map = get_field('map', 'option')) :

    $street      = get_field('street', 'option');
    $suburb      = get_field('suburb', 'option');
    $state       = get_field('state', 'option');
    $postcode    = get_field('postcode', 'option');
    $phone       = get_field('phone', 'option');
    $email       = get_field('email', 'option');

    wp_enqueue_script('gmap', 'https://maps.googleapis.com/maps/api/js?v=3.exp', array(), false, true);

    ?>

    <div data-module="map" class="module mod-map">

        <div class="acf-map">
            <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>">

                <div itemscope itemtype="http://schema.org/Organization">

                    <div class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <p>
                            <span itemprop="streetAddress"><?php echo $street; ?></span><br />
                            <span itemprop="addressLocality"><?php echo $suburb; ?></span>
                            <span itemprop="addressRegion"><?php echo $state; ?></span>
                            <span itemprop="postalCode"><?php echo $postcode; ?></span>
                        </p>

                        <p>
                            <a href="tel:<?php echo str_replace(array('(', ')', ' '), '', $phone); ?>"><span itemprop="telephone"><?php echo $phone; ?></span></a><br />
                            <a href="mailto:<?php echo antispambot($email); ?>"><?php echo antispambot($email); ?></a>
                        </p>
                    </div>

                </div>
            </div>
        </div>

    </div>
<?php endif; ?>