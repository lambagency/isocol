"use strict";

(function( $ ){

    var moduleID = 'map',   // Must equal (data-module)
        module;             // Holds module element

    var map;


    var moduleFunctions = {

        init: function() {

            module.find('.acf-map').each(function() {

                if (typeof google !== 'undefined') {
                    moduleFunctions.render_map( $(this) );
                }
            });
        },

        /**
         *  This function will render a Google Map onto the selected jQuery element
         *
         *  @param	$el (jQuery element)
         *  @return	n/a
         */
        render_map: function( $el ) {

            // var
            var $markers = $el.find('.marker');

            // vars
            var args = {
                zoom		        : 15,
                center		        : new google.maps.LatLng(0, 0),
                mapTypeId	        : google.maps.MapTypeId.ROADMAP,
                scrollwheel         : false,
                streetViewControl   : false,
                mapTypeControl      : false,
                panControl          : false,
                zoomControl         : false,
                scaleControl        : false
            };

            // create map
            map = new google.maps.Map( $el[0], args);

            // add a markers reference
            map.markers = [];

            // add markers
            $markers.each(function(){
                moduleFunctions.add_marker( $(this), map );
            });

            // center map
            moduleFunctions.center_map( map );

            google.maps.event.addDomListener(window, "resize", function() {
                google.maps.event.trigger(map, "resize");
                moduleFunctions.center_map(map);
            });

        },

        /**
         *  This function will add a marker to the selected Google Map
         *
         *  @param	$marker (jQuery element)
         *  @param	map (Google Map object)
         *  @return	n/a
         */
        add_marker: function( $marker, map ) {

            // var
            var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

            // create marker
            var marker = new google.maps.Marker({
                position	: latlng,
                map			: map,
                animation   : google.maps.Animation.DROP
            });


            // add to array
            map.markers.push( marker );

            // if marker contains HTML, add it to an infoWindow
            if( $marker.html() )
            {
                // create info window
                var infowindow = new google.maps.InfoWindow({
                    content		: $marker.html()
                });

                // show info window when marker is clicked
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open( map, marker );
                });
            }

        },

        /**
         *  This function will center the map, showing all markers attached to this map
         *
         *  @param	map (Google Map object)
         *  @return	n/a
         */
        center_map: function(map) {

            // vars
            var bounds = new google.maps.LatLngBounds();

            // loop through all markers and create bounds
            $.each( map.markers, function( i, marker ){

                var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

                bounds.extend( latlng );
            });


            if(map.markers.length == 1 ) {
                map.setCenter( bounds.getCenter() );
                map.setZoom( 16 );
            }
            else {
                map.fitBounds(bounds);
            }
        },


        toggleBounce: function(marker) {
            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            }
            else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );