<?php

$additionalFields = array(
    'hideMobile' => 'hide_mobile'
);

$fields =  LambAgency\Util\getFields($additionalFields);

$logos  = get_sub_field('logos');


// Module Class
//--------------------------

$fields['content'] ? false : ($fields['moduleClass'][] = 'double-padding');

?>

<div data-module="logos" class="module mod-logos <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

        <?php include(locate_template('layouts/components/title-block.php')); ?>


        <div class="row">
            <?php $i = 1; ?>
            <?php foreach ($logos as $logo) : ?>
                    <?php if ($logo['image']) : ?>
                        <div class="img col col-1-4 animated" <?php addAnimation('fadeIn', ($i * 400)); ?>>
                            <a href="<?php echo $logo['link']; ?>" target="_blank">
                                <img src="<?php echo $logo['image']['sizes']['square-medium']; ?>" alt="logo" width="<?php echo $logo['image']['sizes']['square-medium-width']; ?>" height="<?php echo $logo['image']['sizes']['square-medium-height']; ?>" />
                            </a>
                        </div>
                    <?php endif; ?>
             <?php $i++; ?>
            <?php endforeach; ?>
        </div>

    </div>

</div>