<?php include_once(ABSPATH . 'wp-admin/includes/plugin.php'); ?>


<div data-module="sitemap" class="module mod-sitemap">
    <div class="layoutwidth">
        <?php if (is_plugin_active('html-sitemap/html-sitemap.php')) : ?>
            <?php echo do_shortcode('[html_sitemap]'); ?>
        <?php endif; ?>
    </div>
</div>