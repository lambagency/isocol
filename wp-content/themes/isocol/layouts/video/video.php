<?php
$title      = get_sub_field('title');
$iframe     = get_sub_field('video');


// use preg_match to find iframe src
preg_match('/src="(.+?)"/', $iframe, $matches);
$src = $matches[1];


// add extra params to iframe src
$params = array(
	'controls'      => 1,
	'autohide'      => 1,
	'showinfo'      => 0
);

$new_src = add_query_arg($params, $src);

$iframe = str_replace($src, $new_src, $iframe);


// add extra attributes to iframe html
$attributes = 'frameborder="0"';

$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

?>

<div data-module="video" class="module mod-video has-bg no-padding">

	<div class="row grid-no-gutter">

		<div class="col col-1-3" data-mh="video">

			<div class="inner">
				<div class="vert-bot">
					<div class="subtitle">Video</div>

					<?php if($title) : ?>
						<h3><?php echo $title; ?></h3>
					<?php endif; ?>
				</div>
			</div>

		</div>


		<div class="col col-2-3" data-mh="video">

			<?php if($iframe) : ?>
				<div class="video-wrap">
					<?php echo $iframe; ?>
				</div>
			<?php endif; ?>

		</div>

	</div>

</div>