<?php

$fields       =  LambAgency\Util\getFields();
$productUses  = get_sub_field('product_use');

?>

<div data-module="product-uses" class="module mod-product-uses <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

        <?php include(locate_template('layouts/components/title-block.php')); ?>


            <div class="product-uses row">

                <?php $alt = false; ?>

                <?php foreach ($productUses as $use) : ?>
                    <div class="product-row">


                        <div class="images col animated<?php echo $alt ? ' alt' : ''; ?>" <?php addAnimation('fadeIn', 400); ?>>

                            <div class="prod-image">
                                <?php if ($use['product_image']) : ?>
                                    <div class="product-image-1 animated" style="background-image: url(<?php echo $use['product_image']['sizes']['module-bg']; ?>)" <?php addAnimation('fadeIn', 1000); ?>></div>
                                <?php endif; ?>
                            </div>


                            <div class="card">

                                <?php if ($use['cards_image']) : ?>
                                    <div class="card-image" style="background-image: url(<?php echo $use['cards_image']['sizes']['product-card']; ?>)"></div>
                                <?php endif; ?>

                                <?php if ($use['card_summary']) : ?>
                                    <div class="text">
                                        <?php echo $use['card_summary']; ?>
                                    </div>
                                <?php endif; ?>

                            </div>


                            <div class="card-1">
                                <?php if ($use['card_image_1']) : ?>
                                    <div class="card-image" style="background-image: url(<?php echo $use['card_image_1']['sizes']['product-card']; ?>)"></div>
                                <?php endif; ?>

                                <?php if ($use['card_summary_1']) : ?>
                                    <div class="text">
                                        <?php echo $use['card_summary_1']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                        </div>



                        <div class="summary col animated<?php echo $alt ? ' alt' : ''; ?>"  <?php addAnimation('fadeIn', 400); ?>>

                            <?php if ($use['title']) : ?>
                                <h3><?php echo $use['title']; ?></h3>
                            <?php endif; ?>

                            <?php if ($use['summary']) : ?>
                                <?php echo $use['summary']; ?>
                            <?php endif; ?>

                            <?php if ($use['link']) : ?>
                                <a href="<?php echo $use['link']; ?>"><?php echo $use['link_title']; ?></a>
                            <?php endif; ?>

                        </div>


                        <?php $alt = !$alt; ?>
                    </div>
                <?php endforeach; ?>

             </div>






        <?php include(locate_template('layouts/components/content.php')); ?>

        <?php include(locate_template('layouts/components/buttons.php')); ?>

    </div>

</div>