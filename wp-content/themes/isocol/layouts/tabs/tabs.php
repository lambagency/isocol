<?php

$fields = LambAgency\Util\getFields();
$tabs   = get_sub_field('tabs');
?>

<div data-module="tabs" class="module mod-tabs <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

        <?php include(locate_template('layouts/components/title-block.php')); ?>

        <?php include(locate_template('layouts/components/content.php')); ?>


        <?php if (have_rows('tabs')) : ?>

            <div class="row">

                <ul class="tabs tabs-desktop<?php echo $fields['type'] == 'vertical' ? ' col col-1-3' : ''; ?>">

                    <?php $index = 0; ?>

                    <?php while (have_rows('tabs')) : the_row(); ?>

                        <?php $tabItemClass = $index == 0 ? ' first open' : ''; ?>

                        <li data-tab-item="<?php echo $index; ?>" class="tab<?php echo $tabItemClass; ?>">
                            <?php if ($tabTitle = get_sub_field('title')) : ?>
                                <div class="tab-title"><?php echo $tabTitle; ?></div>
                            <?php endif; ?>
                        </li>

                        <?php $index++; ?>

                    <?php endwhile; ?>
                </ul>



                <div class="tab-panels<?php echo $fields['type'] == 'vertical' ? ' col col-2-3' : ''; ?>">

                    <?php $index = 0; ?>

                    <?php while (have_rows('tabs')) : the_row(); ?>

                        <?php $tabItemClass = $index == 0 ? ' first open' : ''; ?>

                        <ul class="tabs tabs-mobile">
                            <li data-tab-item="<?php echo $index; ?>" class="tab<?php echo $tabItemClass; ?>">
                                <?php if ($tabTitle = get_sub_field('title')) : ?>
                                    <div class="tab-title"><?php echo $tabTitle; ?></div>
                                <?php endif; ?>
                            </li>
                        </ul>

                        <div class="panel<?php echo $tabItemClass; ?>">
                            <?php if ($tabContent = get_sub_field('content')) : ?>
                                <div class="content"><?php echo $tabContent; ?></div>
                            <?php endif; ?>

                            <?php getFlexibleContent('tab_modules', true); ?>
                        </div>

                        <?php $index++; ?>

                    <?php endwhile; ?>
                </div>

            </div>

        <?php endif; ?>



        <?php include(locate_template('layouts/components/buttons.php')); ?>

    </div>

</div>