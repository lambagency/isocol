"use strict";

(function( $ ){

    var moduleID = 'tabs',  // Must equal (data-module)
        module;             // Holds module element


    var moduleFunctions = {

        init: function() {

            var animationTime   = 200,
                tabs            = module.find('.tab'),
                panels          = module.find('.panel'),
                desktopTabs     = module.find('.tabs-desktop'),
                tabPanels       = module.find('.tab-panels');


            // if window > tablet portrait, set first tab/panel to 'open'
            if ($.device('min', 'tablet-portrait')) {
                desktopTabs.find('.tab').eq(0).addClass('open');
                tabPanels.find('.tab').eq(0).addClass('open');
                panels.eq(0).addClass('open');
            }


            // on window resize, if 'tablet-portrait' and no tabs are opened, default
            // the first tab to open
            $(window).resize(function() {

                if ($.device('min', 'tablet-portrait')) {

                    var openedTabs = module.find('.tab.open').length;

                    if (openedTabs == 0) {
                        desktopTabs.find('.tab').eq(0).addClass('open');
                        tabPanels.find('.tab').eq(0).addClass('open');
                        panels.eq(0).addClass('open');
                    }
                }
            });


            // click event for the tabs
            tabs.click(function(e) {
                e.preventDefault();

                var currTab = $(this);

                if (!currTab.hasClass('animating')) {

                    var tabIndex        = currTab.data('tab-item'),
                        currDesktopTab  = desktopTabs.find('.tab').eq(tabIndex),
                        currMobileTab   = tabPanels.find('.tab').eq(tabIndex);

                    // prevent multiple tabs to be opened at same time
                    if (currDesktopTab.hasClass('open') && currMobileTab.hasClass('open')) {

                        // only collapse for less than tablet-landscape
                        if ($.device('max', 'tablet-landscape')) {
                            currDesktopTab.removeClass('open');
                            currMobileTab.removeClass('open');
                            panels.removeClass('open');
                        }
                    }
                    else {

                        // reset tabs
                        tabs.removeClass('open');
                        panels.removeClass('open');

                        // set open tab/panel
                        currDesktopTab.addClass('open');
                        currMobileTab.addClass('open');

                        setTimeout(function () {
                            currTab.removeClass('animating');

                            // show active content
                            panels.eq(tabIndex).addClass('open');

                        }, animationTime);
                    }
                }

                // trigger resize event to fix tab issue
                window.dispatchEvent(new Event('resize'));
            });
        
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );