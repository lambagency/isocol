"use strict";

(function( $ ){

    var moduleID = 'accordion', // Must equal (data-module)
        module;                 // Holds module element

    var moduleFunctions = {

        init: function() {

            if (module.hasClass('default')) {
                this.initDefaultAccordion();
            }
            if (module.hasClass('vertical')) {
                this.initVerticalAccordion();
            }
        },

        initDefaultAccordion: function () {

            var animationTime = 200, // Accordion open/close animation time (Milliseconds) should match scss $accordionAnimationDuration
                accordionItems = module.find('.accordion-title');

            accordionItems.click(function(e) {
                e.preventDefault();

                var currItemParent = $(this).parent();

                if (!currItemParent.hasClass('animating')) {

                    currItemParent.addClass('animating');
                    currItemParent.toggleClass('open');

                    setTimeout(function() {
                        currItemParent.removeClass('animating');
                    }, animationTime);

                }
            });
        },


        initVerticalAccordion: function () {

            var animationTime   = 200, // Accordion open/close animation time (Milliseconds) should match scss $accordionAnimationDuration
                accordionItems  = module.find('.accordion-item'),
                accordionTitle  = module.find('.accordion-title'),
                accordionDetail = module.find('.accordion-detail'),
                contentList     = module.find('.accordion-list-wrap'),
                contentWrap     = module.find('.accordion-content-wrap');


            accordionTitle.click(function(e) {
                e.preventDefault();

                var currItemParent = $(this).parent();

                if (!currItemParent.hasClass('animating')) {

                    // get index of selected item
                    var index = currItemParent.data('accordion-item');

                    // get selected accordion items (both views)
                    var currItemList    = contentList.find('.accordion-item').eq(index);
                    var currItemContent = contentWrap.find('.accordion-item').eq(index);

                    // prevent multiple accordion items to be opened at same time
                    if (currItemList.hasClass('open') && currItemContent.hasClass('open')) {

                        // only collapse for less than tablet-landscape
                        if ($.device('max', 'tablet-landscape')) {
                            currItemList.removeClass('open');
                            currItemContent.removeClass('open');
                        }
                    }
                    else {

                        // reset opened items
                        accordionItems.removeClass('open');
                        accordionDetail.removeClass('open');

                        // set active item
                        currItemList.addClass('open');
                        currItemContent.addClass('open');


                        setTimeout(function() {
                            currItemList.removeClass('animating');
                            currItemContent.removeClass('animating');

                            // show active content
                            currItemContent.find('.accordion-detail').addClass('open');

                        }, animationTime);
                    }
                }
            });


            // on window resize, if 'tablet-landscape' and no accordion items are opened, default
            // the first accordion item to open
            $(window).resize(function() {

                if ($.device('min', 'tablet-landscape')) {

                    var openedAccordion = module.find('.accordion-item.open').length;

                    if (openedAccordion == 0) {

                        // set first accordion to open
                        contentList.find('.accordion-item').eq(0).addClass('open');
                        contentWrap.find('.accordion-item').eq(0).addClass('open').find('.accordion-detail').addClass('open');
                    }
                }
            });
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );