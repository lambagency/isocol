<?php

$fields = LambAgency\Util\getFields();
$items = get_sub_field('items');

?>


<div data-module="accordion" class="module mod-accordion <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

        <?php include(locate_template('layouts/components/title-block.php')); ?>

        <?php include(locate_template('layouts/accordion/accordion-' . $fields['type'] . '-partial.php')); ?>

        <?php include(locate_template('layouts/components/buttons.php')); ?>

    </div>
</div>