"use strict";

(function( $ ){

    var moduleID = 'form',  // Must equal (data-module)
        module;             // Holds module element

    var moduleFunctions = {

        init: function() {

            this.initSelects();
            this.initFormValidation();
        },


        /**
         * Adds a wrapper class to the select's parent div to apply a pseudo element
         * to display a custom down arrow
         * Also adds asterisk to required select fields
         */
        initSelects: function()
        {
            module.find('select').each(function() {
                $(this).parent().addClass('select-wrap');

                var currSelect = $(this);
                var li = currSelect.closest('li');

                if (li.hasClass('gfield_contains_required')) {

                    var firstOption = currSelect.find('option:first-child');
                    var optionText = firstOption.text();
                    firstOption.html(optionText + '<span>*</span>');
                }
            });
        },


        /**
         * Switch statement to initialise JS validation for each form
         */
        initFormValidation: function()
        {
            switch(module.attr('id')) {

                case 'formSubscribe':
                    moduleFunctions.subscribeFormValidation('#gform_1');
                    break;

                case 'formEnquiry':
                    moduleFunctions.contactFormValidation('#gform_2');
                    break;


                case 'formWholesale':
                    moduleFunctions.contactFormValidation('#gform_4');
                    break;

                default:
            }
        },


        subscribeFormValidation: function(formID)
        {
            $(formID).validate({
                rules: {
                    input_1: { required: true, email: true }
                }
            });
        },


        contactFormValidation: function(modID)
        {
            $(modID).validate({
                rules: {
                    input_1: { required: true },
                    input_2: { required: true },
                    input_3: { required: true },
                    input_4: { required: true },
                    input_5: { required: true }
                }
            });
        },

        wholesaleFormValidation: function(modID)
        {
            $(modID).validate({
                rules: {
                    input_1: { required: true },
                    input_2: { required: true },
                    input_3: { required: true },
                    input_4: { required: true },
                    input_5: { required: true }
                }
            });
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );