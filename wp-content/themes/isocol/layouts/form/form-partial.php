<?php

$form   = array();
$fields = array();

if (have_rows('forms', 'option')) :

    while (have_rows('forms', 'option')) :

        the_row();

        $formID = get_sub_field('form_id');

        // Check for matching form
        if ($formType == $formID) :

            $form = array(
                'id'               => $formID,
                'form'             => get_sub_field('form'),
                'ajax'             => get_sub_field('form-ajax')
            );

            $fields = array(
                'subtitle'         => get_sub_field('form-subtitle'),
                'title'            => get_sub_field('form-title'),
                'background_image' => get_sub_field('form-background-image')
            );

        endif;
    endwhile;
endif;



if ($form) :

    $fields['bgImage']          = $fields['background_image'];
    $fields['bgImageURL']       = $fields['bgImage'] ? $fields['bgImage']['sizes']['module-bg'] : '';
    $fields['bgImageMobileURL'] = $fields['bgImage'] ? $fields['bgImage']['sizes']['module-bg-mobile'] : '';
?>

    <div data-module="form" id="form<?php echo ucfirst($form['id']); ?>" class="module mod-form mod-form-<?php echo $form['id']; ?> center<?php echo $fields['bgImage'] ? ' has-bg hide-bg-mobile' : ''; ?>">

        <?php include(locate_template( 'layouts/components/background-image.php' )); ?>

        <div class="layoutwidth">

            <?php include(locate_template( 'layouts/components/title-block.php' )); ?>


            <?php if ($form['form']) :
                $gravityForm = gravity_form($form['form']['id'], false, false, false, '', $form['ajax'], 1, false);
                echo str_replace('<li', ' <li', $gravityForm); // Add space to list items to fix grid margin right bug
            endif; ?>

        </div>
    </div>

<?php endif;