"use strict";

(function( $ ){

    var moduleID = 'banner', // Must equal (data-module)
        module;              // Holds module element


    var moduleFunctions = {

        init: function() {
            
            if (module.hasClass('video-banner')) {
                this.initVideoBanner();
            }
            else {
                this.initImageBanner();
            }
        },

        initVideoBanner: function() {
    
            if (!themeConfig.support.html5Video) {
    
                module.addClass('no-video');
    
                lazyImages.refresh();
            }
        },

    
        initSlideBeforeChange: function(slider, currentSlideIndex, nextSlideIndex)
        {
            // Current Slide
            //-----------------------
    
            var currentSlide            = slider.$slides[currentSlideIndex],
                currentAnimatedChildren = $(currentSlide).find('.animated');
    
            $.each(currentAnimatedChildren, function () {
                if (themeConfig.isDesktop()) {
                    $(this).customAnimate('hide');
                }
            });
    
    
            // Next Slide
            //-----------------------
    
            var nextSlide               = slider.$slides[nextSlideIndex],
                nextAnimatedChildren    = $(nextSlide).find('.animated');
    
            $.each(nextAnimatedChildren, function() {
                if (themeConfig.isDesktop()) {
                    $(this).customAnimate();
                }
                else {
                    $(this).css('visibility', 'visible');
                }
            });
        },
    
    
        initSlideChange: function(slider)
        {
            var firstLoadDelay = 400; // Delay slide contents loading for first load slide (Milliseconds)
    
            setTimeout(function() {
                moduleFunctions.initSlideBeforeChange(slider, 0, 0);
            }, firstLoadDelay);
        },


        initImageBanner: function ()
        {
            var slider = module.find('.slider'),
                slides = slider.find('.banner');

            if (slider.length && (slides.length > 1)) {

                slider.on('init', function(event, slick) {
                    moduleFunctions.initSlideChange(slick);
                });

                slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
                    moduleFunctions.initSlideBeforeChange(slick, currentSlide, nextSlide);
                });

                slider.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                    infinite: true,
                    autoplay: true,
                    arrows: false,
                    dots: true,
                    adaptiveHeight: false,
                    autoplaySpeed: 8000
                });
            }
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );