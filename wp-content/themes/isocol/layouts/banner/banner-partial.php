<?php if ($banner) :

    // Module Class
    //---------------------------------

    $moduleClass = $banner['size'] ? ' size-' . $banner['size'] : ' size-compact'; // Banner size, default to compact
    $moduleClass .= ($banner['type'] == 'video') ? ' video-banner' : ' image-banner';
    ?>
    <div data-module="banner" class="module mod-banner has-bg<?php echo $moduleClass; ?>">

        <div class="slider">

            <?php foreach ($banner['slides'] as $key => $slide) : ?>

                <?php

                // Titles
                //---------------------------------

                $subtitle       = (isset($slide['subtitle']) && is_string($slide['subtitle'])) ? $slide['subtitle'] : false;
                $title          = (isset($slide['title']) && is_string($slide['title'])) ? $slide['title'] : false;
                $title          = $title && $title != '' ? $title : get_the_title();


                // Background Images
                //---------------------------------

                $bgImageURL         = isset($slide['background_image_url']) ? $slide['background_image_url'] : false;
                $bgImageMobileURL   = isset($slide['background_image_mobile_url']) ? $slide['background_image_mobile_url'] : false;

                if (!$bgImageURL || !$bgImageMobileURL) {

                    $bgImage    = (isset($slide['background_image']) && is_array($slide['background_image'])) ? $slide['background_image'] : false; // Load background image
                    $bgImage    = $bgImage ? : get_field('background_image', 'option'); // Default background image fallback

                    $bgImageURL         = $bgImageURL ? : ($bgImage ? $bgImage['sizes']['banner-bg'] : false);
                    $bgImageMobileURL   = $bgImageMobileURL ? : ($bgImage ? $bgImage['sizes']['banner-bg-mobile'] : false);
                }


                // Video
                //---------------------------------

                $bannerMP4URL   = (isset($slide['video_mp4'])&& is_array($slide['video_mp4'])) ? $slide['video_mp4']['url'] : '';
                $bannerWEBMURL  = (isset($slide['video_webm'])&& is_array($slide['video_webm'])) ? $slide['video_webm']['url'] : '';
                $bannerOGGURL   = (isset($slide['video_ogg'])&& is_array($slide['video_ogg'])) ? $slide['video_ogg']['url'] : '';


                // Buttons
                //---------------------------------

                $fields['buttons'] = array(
                    'buttons' => isset($slide['buttons']) ? $slide['buttons'] : false
                );


                // Banner Class
                //---------------------------------

                $bannerClass = $bgImageURL ? ' has-bg' : '';


                // Animation
                //---------------------------------

                $subtitleAnimationDelay     = 200;
                $titleAnimationDelay        = $subtitle ? 400 : 200;
                $fields['buttons']['delay'] = $subtitle ? 700 : 500;


                $hold                       = sizeof($banner['slides']) > 1 ? 'data-animate-hold' : '';
                $fields['buttons']['hold']  = $hold ? true : false;

                ?>

                <div class="banner<?php echo $bannerClass; ?>">

                    <?php if ($banner['type'] == 'video') : ?>
                        <div class="video-bg animated" <?php addAnimation('fadeIn'); ?>>
                            <video autoplay loop muted>
                                <?php if ($bannerMP4URL) : ?><source src="<?php echo $bannerMP4URL; ?>" type="video/mp4"><?php endif; ?>
                                <?php if ($bannerWEBMURL) : ?><source src="<?php echo $bannerWEBMURL; ?>" type="video/webm"><?php endif; ?>
                                <?php if ($bannerOGGURL) : ?><source src="<?php echo $bannerOGGURL; ?>" type="video/ogg"><?php endif; ?>
                            </video>
                        </div>
                    <?php endif; ?>

                    <div class="banner-bg lazy-image" data-lazy-parent data-src="<?php echo $bgImageURL; ?>" data-src-mobile="<?php echo $bgImageMobileURL; ?>"></div>

                    <div class="layoutwidth">
                        <div class="inner">

                            <?php if($subtitle) : ?>
                                <div class="subtitle animated" <?php echo $hold; ?> <?php addAnimation('fadeInDown', $subtitleAnimationDelay); ?>><?php echo $subtitle; ?></div>
                            <?php endif; ?>


                            <?php if($title) : ?>
                                <?php if ($key > 0) : ?>
                                    <div class="h1 title animated" <?php echo $hold; ?> <?php addAnimation('fadeIn', $titleAnimationDelay); ?>><?php echo $title; ?></div>
                                <?php else : ?>
                                    <h1 class="title animated" <?php echo $hold; ?> <?php addAnimation('fadeIn', $titleAnimationDelay); ?>><?php echo $title; ?></h1>
                                <?php endif; ?>
                            <?php endif; ?>


                            <?php include(locate_template( 'layouts/components/buttons.php' )); ?>


                            <div class="banner-products animated" <?php addAnimation('fadeInDown', 600); ?>>
                                <?php if ($bannerProduct = get_field('banner_product', 'option')) : ?>
                                    <img src="<?php echo $bannerProduct['sizes']['product-collection']; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $bannerProduct['sizes']['product-collection-width']; ?>" height="<?php echo $bannerProduct['sizes']['product-collection-height']; ?>" />
                                <?php endif; ?>
                            </div>


                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>