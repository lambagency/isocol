<?php

$banner = [];
$banner['size'] = get_sub_field('size');


$bannerType = get_sub_field('banner_type');

// Video Banner
//-------------------------
if ($bannerType == 'video') {

    $slide = array(
        'subtitle'      => 'Subtitle',
        'title'         => 'Test Video',
        'video_mp4'     => get_sub_field('video_mp4'),
        'video_webm'    => get_sub_field('video_webm'),
        'video_ogg'     => get_sub_field('video_ogg')
    );

    $banner['type']     = 'video';
    $banner['slides']   = array($slide); // Assign video slide
}
// Image Banner
//-------------------------
else {

    $banner['type']     = 'image';

    // Multiple slides
    if ($slides = get_sub_field('slides')) {
        $banner['slides'] = $slides;
    }
    // Single slide
    else {

        $slide = [];

        // Blog listing Page
        if (is_home() || is_category()) {
            $slide['title']     = get_the_title(get_option('page_for_posts')); // Blog listing page title
            $bannerImage        = get_field('blog_image', 'option');
        }
        // Custom Post Type Archive Page
        elseif (is_post_type_archive()) {
            $slide['title']     = post_type_archive_title('', false);
        }
        // Single Blog Page
        elseif (is_singular('post')) {
            $slide['subtitle']  = '';
            $slide['title']     = '';
            $bannerImage        = get_field('blog_image', 'option');

            $useFeatureImage = get_field('feature_image');

            // Check if feature image should be used
            if (isset($useFeatureImage) && $useFeatureImage) {
                $slide['background_image_url']          = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'banner-bg')[0];
                $slide['background_image_mobile_url']   = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'banner-bg-mobile')[0];
            }
        }
        // Search Results page
        elseif (is_search()) {
            $slide['title'] = 'Search Results';
        }
        // 404 Page
        elseif (is_404()) {
            $slide['title']     = '404';
        }
        // All Other Pages
        else {
            $slide['title']     = get_the_title();
        }


        // Banner Image optional overrides
        $bannerImage                = (isset($bannerImage) && $bannerImage) ? $bannerImage : get_field('banner_image');
        $slide['background_image']  = $bannerImage ? : false;

        $banner['slides'] = array($slide); // Assign slide
    }
}

include(locate_template( 'layouts/banner/banner-partial.php' ));

include(locate_template( 'layouts/breadcrumbs/breadcrumbs.php' ));