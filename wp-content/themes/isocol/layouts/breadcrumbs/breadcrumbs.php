<?php if(!is_front_page()) : ?>
    <?php if(function_exists('yoast_breadcrumb')) : ?>
        <div class="breadcrumbs arrows">
            <div class="layoutwidth">
                <?php yoast_breadcrumb(); ?>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>