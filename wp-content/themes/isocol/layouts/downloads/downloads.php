<?php

$fields =  LambAgency\Util\getFields();

$downloads      = get_sub_field('downloads');
$downloadsNote  = get_sub_field('download_note');

function getIconClass($mimeType)
{
    switch ($mimeType) {

        case 'application/pdf' :
            $downloadIconClass = 'fa-file-pdf-o';
            break;

        case 'text/plain' :
            $downloadIconClass = 'fa-file-text-o';
            break;

        case 'doc' :
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' :
            $downloadIconClass = 'fa-file-word-o';
            break;

        case 'application/zip' :
            $downloadIconClass = 'fa-file-zip-o';
            break;

        default :
            $downloadIconClass = 'fa-arrow-down';
    }

    return $downloadIconClass;
}

?>

<div data-module="downloads" class="module mod-downloads">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth center">

        <?php include(locate_template('layouts/components/title-block.php')); ?>

        <?php include(locate_template('layouts/components/content.php')); ?>


        <?php if ($downloads) : ?>
            <div class="downloads">

                <?php foreach ($downloads as $key => $download) : ?>

                    <?php

                    $file       = $download['file'];
                    $fileTitle  = $file['title'];
                    $fileUrl    = $file['url'];
                    $fileType   = $file['mime_type'];

                    $fileTitle  = $download['title'] ? $download['title'] : $fileTitle;

                    // Classes
                    $downloadItemClass = $key == 0 ? ' first' : '';
                    $downloadIconClass = getIconClass($fileType);

                    ?>

                    <a href="<?php echo $fileUrl; ?>" target="_blank" download="download" class="download<?php echo $downloadItemClass; ?>">
                        <?php echo $fileTitle; ?>

                        <i class="download-icon fa <?php echo $downloadIconClass; ?>"></i>
                    </a>

                <?php endforeach; ?>

                <?php if($downloadsNote) : ?>
                    <div class="download-note"><?php echo $downloadsNote; ?></div>
                <?php endif; ?>

            </div>
        <?php endif; ?>

    </div>

</div>