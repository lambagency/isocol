"use strict";

(function( $ ){

    var moduleID = 'footer', // Must equal (data-module)
        module;              // Holds module element


    var moduleFunctions = {

        init: function () {

            var links       = $('.social-media a');
            var wrap        = $('.social-hover');

            if (links.length) {
                links.hover(
                    function() {

                        var e       = $(this);
                        var title   = '<h4>' + e.data('social-msg-title') + '</h4>';
                        var message = '<p>' + e.data('social-msg-value') + '</p>';

                        wrap.html(title + message).addClass('show');
                    },
                    function() {
                        wrap.empty().removeClass('show');
                    }
                );
            }
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
    module = $(this);
    moduleFunctions.init();
};

})( jQuery );