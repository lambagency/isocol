    <footer id="footer" role="contentinfo">

        <?php if ($footerLogo = get_field('footer_logo', 'option')) : ?>
            <a href="<?php echo BASE_URL; ?>" class="footer-logo">
                <img src="<?php echo $footerLogo['sizes']['footer-logo']; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $footerLogo['sizes']['footer-logo-width']; ?>" height="<?php echo $footerLogo['sizes']['footer-logo-height']; ?>" />
            </a>
        <?php endif; ?>

        <div class="footer-products row">
            <div class="product">
                <?php if ($footerProduct = get_field('footer_product', 'option')) : ?>
                    <div class="footer-product">
                        <a href="<?php echo get_field('product_link', 'option') ?>">
                            <img src="<?php echo $footerProduct['sizes']['footer-product']; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $footerProduct['sizes']['footer-product-width']; ?>" height="<?php echo $footerProduct['sizes']['footer-product-height']; ?>" />
                        </a>
                    <?php if ($productTitle = get_field('product_title', 'option')) : ?>
                        <p><?php echo $productTitle; ?></p>
                    <?php endif; ?>

                    </div>
                <?php endif; ?>
            </div>

            <div class="product">
                <?php if ($footerProduct1 = get_field('footer_product_1', 'option')) : ?>
                    <div class="footer-product">
                        <a href="<?php echo get_field('product_link_1', 'option') ?>">
                            <img src="<?php echo $footerProduct1['sizes']['footer-product']; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $footerProduct1['sizes']['footer-product-width']; ?>" height="<?php echo $footerProduct1['sizes']['footer-product-height']; ?>" />
                        </a>
                        <?php if ($productTitle1 = get_field('product_title_1', 'option')) : ?>
                            <p><?php echo $productTitle1; ?></p>
                        <?php endif; ?>

                    </div>
                <?php endif; ?>
            </div>

            <div class="product">
                <?php if ($footerProduct2 = get_field('footer_product_2', 'option')) : ?>
                    <div class="footer-product">
                        <a href="<?php echo get_field('product_link_2', 'option') ?>">
                            <img src="<?php echo $footerProduct2['sizes']['footer-product']; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $footerProduct2['sizes']['footer-product-width']; ?>" height="<?php echo $footerProduct2['sizes']['footer-product-height']; ?>" />
                        </a>
                        <?php if ($productTitle2 = get_field('product_title_2', 'option')) : ?>
                            <p><?php echo $productTitle2; ?></p>
                        <?php endif; ?>

                    </div>
                <?php endif; ?>
            </div>

            <div class="product isocol-face">
                <?php if ($footerProduct3 = get_field('footer_product_3', 'option')) : ?>
                    <div class="footer-product">
                        <a href="<?php echo get_field('product_link_3', 'option') ?>">
                            <img src="<?php echo $footerProduct3['sizes']['footer-product']; ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo $footerProduct3['sizes']['footer-product-width']; ?>" height="<?php echo $footerProduct3['sizes']['footer-product-height']; ?>" />
                        </a>
                        <?php if ($productTitle3 = get_field('product_title_3', 'option')) : ?>
                            <p><?php echo $productTitle3; ?></p>
                        <?php endif; ?>

                    </div>
                <?php endif; ?>
            </div>

        </div>


        <div class="top">

            <div class="layoutwidth">

                <div class="footer-nav block">
                    <?php
                    if (has_nav_menu('footer')) :
                        wp_nav_menu(array(
                            'theme_location'    => 'footer',
                            'walker'            => new Custom_Walker_Nav_Menu
                        ));
                    endif;
                    ?>
                </div>

                <div class="legals">
                    &copy; <?php bloginfo(); ?>. All rights reserved.

                    <div class="footer-nav inline">
                        <?php
                        if (has_nav_menu('footer-legals')) :
                            wp_nav_menu(array(
                                'theme_location'    => 'footer-legals',
                                'walker'            => new Custom_Walker_Nav_Menu
                            ));
                        endif;
                        ?>
                    </div>


                    | Site by <a href="http://www.lambagency.com.au" target="_blank"<?php echo !is_front_page() ? ' rel="nofollow"' : ''; ?>>Lamb Agency</a>

                </div>

                <div class="disclaimer">
                    <?php echo get_field('disclaimer', 'option') ?>
                </div>

            </div>

        </div>


        <div class="bottom">

            <div class="layoutwidth">

                <?php include(locate_template('layouts/components/social-media-icons-circles.php')); ?>


            </div>

        </div>

    </footer>
</div>

<div class="state-indicator"></div>

<?php wp_footer(); ?>

<?php include(locate_template('layouts/components/remarketing-tags.php')); ?>

</body>
</html>