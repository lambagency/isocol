<?php

$fields =  LambAgency\Util\getFields();

$testimonials   = get_sub_field('testimonials');
?>


<div data-module="testimonials" class="module mod-testimonial <?php echo implode(' ', $fields['moduleClass']); ?>">


    <?php include(locate_template( 'layouts/components/background-image.php' )); ?>


    <div class="layoutwidth center">

        <?php include(locate_template( 'layouts/components/title-block.php' )); ?>

        <?php if($testimonials) : ?>
            <div class="slider">
                <?php foreach ($testimonials as $testimonial) :   ?>

	                <div class="slide">
                       <span class="testimonial-content"><?php echo $testimonial->post_content; ?></span>
	                </div>
	                
                <?php endforeach; ?>
            </div>

            <?php wp_reset_postdata(); ?>
        <?php endif; ?>


	    <?php include(locate_template( 'layouts/components/buttons.php' )); ?>

    </div>
</div>