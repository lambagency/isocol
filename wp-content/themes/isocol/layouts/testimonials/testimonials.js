"use strict";

(function( $ ){

    var moduleID = 'testimonials',
        module;


    var moduleFunctions = {

        init: function() {

            moduleFunctions.initSlider();

        },

        initSlider: function() {
            var slider = module.find('.slider');

            if (slider.length) {
                slider.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true
                })
            }
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );