<?php

$fields =  LambAgency\Util\getFields();

?>

<div data-module="base" class="module mod-base <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image.php')); ?>

    <div class="layoutwidth">

        <?php include(locate_template('layouts/components/title-block.php')); ?>

        <?php include(locate_template('layouts/components/content.php')); ?>

        <?php include(locate_template('layouts/components/buttons.php')); ?>

    </div>

</div>