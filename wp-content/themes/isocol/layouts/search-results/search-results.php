<div data-module="search-results" class="module mod-search-results">

    <div class="results layoutwidth">

        <h2><?php printf(__('Search Results for: \'%s\''), get_search_query()); ?></h2>

        <?php if (have_posts()) : ?>

            <ul>
                <?php while (have_posts()) : the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                <?php endwhile; ?>
            </ul>

        <?php else : ?>

            <p>No posts were found which match the search term.</p>

        <?php endif; ?>

    </div>

</div>