<?php
$facebookURL    = get_field('facebook', 'option');
$twitterURL     = get_field('twitter', 'option');
$linkedInURL    = get_field('linkedin', 'option');
$googlePlusURL  = get_field('google_plus', 'option');
$youTubeURL     = get_field('youtube', 'option');
$instagramURL   = get_field('instagram', 'option');
?>


<div class="social-media-icons">
    <?php if ($facebookURL) : ?>
		<a href="<?php echo $facebookURL; ?>" target="_blank" class="facebook">
			<i class="fa fa-facebook"></i>
		</a>
	<?php endif; ?>

    <?php if ($twitterURL) : ?>
		<a href="<?php echo $twitterURL; ?>" target="_blank" class="twitter">
			<i class="fa fa-twitter"></i>
		</a>
	<?php endif; ?>

    <?php if ($linkedInURL) : ?>
		<a href="<?php echo $linkedInURL; ?>" target="_blank" class="linkedin">
			<i class="fa fa-linkedin"></i>
		</a>
	<?php endif; ?>

    <?php if ($googlePlusURL) : ?>
		<a href="<?php echo $googlePlusURL; ?>" target="_blank" class="google">
			<i class="fa fa-google-plus"></i>
		</a>
	<?php endif; ?>

    <?php if ($youTubeURL) : ?>
		<a href="<?php echo $youTubeURL; ?>" target="_blank" class="youtube">
			<i class="fa fa-youtube"></i>
		</a>
	<?php endif; ?>

    <?php if ($instagramURL) : ?>
		<a href="<?php echo $instagramURL; ?>" target="_blank" class="instagram">
			<i class="fa fa-instagram"></i>
		</a>
	<?php endif; ?>
</div>