<?php if (isset($fields) && $fields['content']) : ?>
    <div class="content">
        <?php echo $fields['content']; ?>
    </div>
<?php endif; ?>