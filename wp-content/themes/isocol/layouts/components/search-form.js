"use strict";

(function( $ ){

    var moduleID = 'search-form',
        module;

    var moduleFunctions = {

        init: function() {

            // relies on http://easyautocomplete.com/
            if ($.isFunction($.fn.easyAutocomplete)) {

                var searchInput = module.find('.search-input');

                var options = {
                    url: autocompleteJS.url,
                    getValue: 'title',
                    template: {
                        type: 'custom',
                        method: function(value, item) {
                            return '<a href="' + item.url + '">' + item.title + '</a>';
                        }
                    },
                    list: {
                        maxNumberOfElements: 10,
                        match: {
                            enabled: true
                        },
                        sort: {
                            enabled: true
                        }
                    }
                };

                searchInput.easyAutocomplete(options);
            }
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );