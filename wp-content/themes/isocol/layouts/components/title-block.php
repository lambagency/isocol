<?php

if (isset($fields) && is_array($fields)) :

    if ($fields['subtitle'] || $fields['title']) : ?>

        <div class="title-block">
            <?php if ($fields['title']) : ?>
                <h2 class="title"><?php echo $fields['title']; ?></h2>
            <?php endif; ?>

            <?php if ($fields['subtitle']) : ?>
                <div class="subtitle"><?php echo $fields['subtitle']; ?></div>
            <?php endif; ?>
        </div>

    <?php endif;

endif;