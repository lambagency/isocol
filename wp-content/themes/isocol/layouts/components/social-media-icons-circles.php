<?php
$facebookURL    = get_field('facebook', 'option');
$twitterURL     = get_field('twitter', 'option');
$linkedInURL    = get_field('linkedin', 'option');
$googlePlusURL  = get_field('google_plus', 'option');
$youTubeURL     = get_field('youtube', 'option');
$instagramURL   = get_field('instagram', 'option');
$pinterestURL   = get_field('pinterest', 'option');
?>


<div class="social-media-icons circles">
	<?php if ($facebookURL) : ?>
		<a href="<?php echo $facebookURL; ?>" target="_blank" class="facebook">
            <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-facebook fa-stack-1x"></i>
            </span>
		</a>
	<?php endif; ?>
    

    <?php if ($pinterestURL) : ?>
        <a href="<?php echo $pinterestURL; ?>" target="_blank" class="pinterest">
            <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-pinterest fa-stack-1x"></i>
            </span>
        </a>
    <?php endif; ?>


    <?php if ($twitterURL) : ?>
		<a href="<?php echo $twitterURL; ?>" target="_blank" class="twitter">
            <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-twitter fa-stack-1x"></i>
            </span>
		</a>
	<?php endif; ?>


	<?php if ($linkedInURL) : ?>
		<a href="<?php echo $linkedInURL; ?>" target="_blank" class="linkedin">
            <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-linkedin fa-stack-1x"></i>
            </span>
		</a>
	<?php endif; ?>


    <?php if ($instagramURL) : ?>
        <a href="<?php echo $instagramURL; ?>" target="_blank" class="instagram">
            <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-instagram fa-stack-1x"></i>
            </span>
        </a>
    <?php endif; ?>


	<?php if ($youTubeURL) : ?>
		<a href="<?php echo $youTubeURL; ?>" target="_blank" class="youtube">
            <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-youtube fa-stack-1x"></i>
            </span>
		</a>
	<?php endif; ?>

    <?php if ($googlePlusURL) : ?>
        <a href="<?php echo $googlePlusURL; ?>" target="_blank" class="google">
            <span class="fa-stack fa-2x">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-google-plus fa-stack-1x"></i>
            </span>
        </a>
    <?php endif; ?>
</div>