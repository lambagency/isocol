<?php

if (isset($fields) && is_array($fields)) :

    $buttons = $fields['buttons'];

    $buttonsShowContainer = isset($buttons['container']) ? $buttons['container'] : true;

    if (isset($buttons) && $buttons['buttons']) : ?>
        <?php if ($buttonsShowContainer) : ?><div class="actions"><?php endif; ?>

        <?php foreach ($buttons['buttons'] as $key => $button) :

            // button attributes
            $type           = isset($button['button_type']) ? $button['button_type'] : 'link';
            $text           = isset($button['text']) ? $button['text'] : '';
            $color          = isset($button['button_colour']) ? ' bg-' . $button['button_colour'] : ' bg-primary';
            $anchor         = isset($button['anchor']) ? $button['anchor'] : false;
            $icon           = isset($button['button_icon']) ? $button['button_icon'] : '';
            $iconPosition   = isset($button['button_icon_position']) ? $button['button_icon_position'] : 'right';
            $url            = '';
            $newTab         = isset($button['new_tab']) ? $button['new_tab'] : false;


            // Animation
            $buttonAnimation    = isset($buttons['animation']) ? $buttons['animation']: 'fadeIn';
            $buttonDelay        = isset($buttons['delay']) ? $buttons['delay'] : 400; // default 400ms delay
            $buttonDelay        += ($key * 200); // Offset for additional buttons

            $hold               =  isset($buttons['hold']) && $buttons['hold'] ? ' data-animate-hold' : '';


            // Page Link (Default)
            if (!$type || $type == 'link') {

                $url    = isset($button['link']) ? $button['link'] : '';
                $url    = $anchor ? $url . '#' . $anchor : $url;

            }
            // External Link
            else if ($type == 'external') {

                $url    = isset($button['external_link']) ? $button['external_link'] : '';
                $url    = $anchor ? $url . '#' . $anchor : $url;
            }
            // Phone
            else if ($type == 'phone') {

                $url    = isset($button['phone']) ? 'tel:' . str_replace(array('(', ')', '-', ' '), '', $button['phone']) : '';
            }
            // Email
            else if ($type == 'email') {

                $url    = isset($button['email']) ? 'mailto:' . $button['email'] : '';
                $newTab = true;
            }
            // File Download
            else if ($type == 'download') {

                $file   = isset($button['file']) ? $button['file'] : false;

                if ($file) {
                    $url    = $file ? $file['url'] : '';
                    $newTab = true;
                    $icon   = 'fa-arrow-down';
                }
            }


            // Classes
            $buttonClass = $color;
            $buttonClass .= $buttonAnimation ? ' animated' : ''; // Only animate if animation set

            // Href
            $link  = $url ? "href=\"{$url}\"" : '';
            $link  .= $newTab ? ' target="_blank"' : '';


            // Icon
            $iconElement = $icon ? '<i class="icon-'. $iconPosition .' fa '. $icon .'"></i>' : '';

            $buttonText = ($iconPosition == 'left' || $iconPosition == 'far-left') ? ($iconElement . $text) : ($text .$iconElement);

            ?>

            <a <?php echo $link; ?>
                class="button bg-hover<?php echo $buttonClass ?>"
                role="button" <?php echo $hold; ?> <?php addAnimation($buttonAnimation, $buttonDelay); ?>>
                <?php echo $buttonText; ?>
            </a>

        <?php endforeach; ?>

        <?php if ($buttonsShowContainer) : ?></div><?php endif; ?>
    <?php endif; ?>
<?php endif; ?>