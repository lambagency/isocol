<div data-module="search-form" class="module mod-search-form">
    <form role="search" method="get" id="searchForm" class="searchform" action="<?php echo BASE_URL; ?>">
        <input type="text" id="txtSearch" name="s" placeholder="Search..." class="search-input" />
    </form>
</div>