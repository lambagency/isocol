<?php

if (isset($fields) && is_array($fields)) : ?>

    <?php if ($fields['bgImageURL']) : ?>
        <div class="module-bg lazy-image" data-src="<?php echo $fields['bgImageURL'];?>" data-src-mobile="<?php echo $fields['bgImageMobileURL']; ?>"></div>
    <?php endif; ?>

<?php endif; ?>