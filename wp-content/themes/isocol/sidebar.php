<aside id="sidebar" role="complementary">
    <?php if (is_singular()) : ?>
        <?php if (is_active_sidebar('sidebar-blog-post-widget')) : ?>
            <div id="primary" class="widget-area">
                <ul>
                    <?php dynamic_sidebar('sidebar-blog-post-widget'); ?>
                </ul>
            </div>
        <?php endif; ?>
    <?php else : ?>
        <?php if (is_active_sidebar('sidebar-blog-list-widget')) : ?>
            <div id="primary" class="widget-area">
                <ul>
                    <?php dynamic_sidebar('sidebar-blog-list-widget'); ?>
                </ul>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</aside>
