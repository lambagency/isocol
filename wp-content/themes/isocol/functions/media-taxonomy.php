<?php

/**
 * Assign a taxonomy term to all ACF Image fields when a post/page is saved
 */
function assignMediaTaxonomyACFImageFields()
{
    if (array_key_exists('acf', $_POST)) {

        assignACFFieldValues(getACFFields('image'), false);
        assignACFFieldValues(getACFFields('gallery'), true);
    }
}

add_action('acf/save_post', 'assignMediaTaxonomyACFImageFields', 10, 3);


function assignACFFieldValues($acfFields, $gallery)
{
    foreach ($_POST['acf'] as $fieldKey => $fieldValue) {

        if (is_array($fieldValue)) {

            if ($gallery) {

                foreach ($fieldValue as $galleryImage) {
                    checkFieldAssignTerm($fieldKey, $galleryImage, $acfFields);
                }

                continue;
            }


            // flexible content layout
            foreach ($fieldValue as $layoutFields) {

                if (is_array($layoutFields)) {

                    // layout fields
                    foreach ($layoutFields as $layoutFieldKey => $layoutFieldValue) {

                        if ($layoutFieldValue == '') {
                            continue;
                        }

                        if (is_array($layoutFieldValue)) {

                            // repeater fields
                            foreach ($layoutFieldValue as $layoutRepeaterValue) {

                                if (is_array($layoutRepeaterValue)) {

                                    // repeater sub fields
                                    foreach ($layoutRepeaterValue as $layoutRepeaterSubKey => $layoutRepeaterSubValue) {
                                        checkFieldAssignTerm($layoutRepeaterSubKey, $layoutRepeaterSubValue, $acfFields);
                                    }
                                }
                            }

                        } else {

                            // single field
                            checkFieldAssignTerm($layoutFieldKey, $layoutFieldValue, $acfFields);
                        }
                    }
                }
            }

        } else {

            // single field
            if ($fieldValue == '') {
                continue;
            }

            checkFieldAssignTerm($fieldKey, $fieldValue, $acfFields);
        }
    }
}


/**
 * Return an array of ACF Image fields
 *
 * @param string $fieldType
 *
 * @return array
 */
function getACFFields($fieldType = 'image')
{
    $acfFields = array();

    // get acf field groups
    $acfGroups = acf_get_field_groups();

    if ($acfGroups) {
        foreach ($acfGroups as $group) {

            // get acf fields for specified group
            $fields = acf_get_fields($group);

            if ($fields) {
                foreach ($fields as $field) {

                    // check if field is a flexible content
                    if ($field['type'] === 'flexible_content') {

                        // loop through each layout in flexible content
                        foreach ($field['layouts'] as $layout) {

                            // loop through each field in layout
                            foreach ($layout['sub_fields'] as $subField) {

                                // check if field is a repeater
                                if ($subField['type'] === 'repeater') {
                                    foreach ($subField['sub_fields'] as $repeaterField) {

                                        // if field type matches, add to array
                                        if ($repeaterField['type'] === $fieldType) {
                                            $acfFields[$repeaterField['key']] = $repeaterField['label'];
                                        }
                                    }
                                } elseif ($subField['type'] === $fieldType) {
                                    $acfFields[$subField['key']] = $subField['label'];
                                }
                            }
                        }
                    }
                    // check if field is a repeater
                    elseif ($field['type'] === 'repeater') {

                        foreach ($field['sub_fields'] as $repeaterField) {

                            // if field type matches, add to array
                            if ($repeaterField['type'] === $fieldType) {
                                $acfFields[$repeaterField['key']] = $repeaterField['label'];
                            }
                        }
                    }
                    // check non-nested field type is what we're after
                    elseif ($field['type'] === $fieldType) {
                        $acfFields[$field['key']] = $field['label'];
                    }
                }
            }
        }
    }

    return $acfFields;
}


/**
 * Checks to see if key is in ACF Image field array. If it is, a taxonomy term relationship is created
 * between the attachment (image) and the term
 *
 * @param $key
 * @param $value
 * @param $acfFields
 */
function checkFieldAssignTerm($key, $value, $acfFields)
{
    $taxonomy = 'media_category';

    if (array_key_exists($key, $acfFields)) {
        $attachmentID = $value;
        wp_set_object_terms($attachmentID, $acfFields[$key], $taxonomy);
    }
}