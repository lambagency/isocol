<?php

/**
 * Looks at the layout folder and picks out the data requested
 *
 * @param null  $name
 * @param null  $key
 * @param array $params
 */
function get_layout( $name = null, $key = null, $params = array()) {
    do_action( 'get_layout', $name );

    $templates = array();
    $name = (string) $name;
    if ( '' !== $name ) {

        if ( '' !== $key ) {
            $templates[] = "layouts/{$name}/{$name}-{$key}.php";
        }

        $templates[] = "layouts/{$name}/{$name}-default.php";
        $templates[] = "layouts/{$name}/{$name}.php";

        if ( '' !== $key ) {
            $templates[] = "layouts/{$name}-{$key}.php";
        }

        $templates[] = "layouts/{$name}.php";
    }

    locate_template($templates, true, false);
}



/**
 * Renders out layouts from specified flexible content field
 *
 * @param $flexibleContent
 * @param bool $tabs
 */
function getFlexibleContent($flexibleContent, $tabs = false)
{
    if (have_rows($flexibleContent)) {

        while (have_rows($flexibleContent)) {

            the_row();

            $layout = get_row_layout();

            // split layout name by underscore
            $layoutComponents = explode('_', $layout, 2);

            // if layout key defined in layout name
            if (count($layoutComponents) > 1) {
                $key = $layoutComponents[1];
            }
            else {
                // Get type from select box
                $type = get_sub_field('type');
                $key = $type ? strtolower(trim($type)) : 'default';
            }

            $name = $layoutComponents[0];
            $key  = $key == 'default' ? '' : $key;

            get_layout($name, $key);
        }

    } else {

        if (!$tabs) {
            get_layout('banner');

            if (defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) {
                get_layout('empty');
            }
        }
    }
}



/**
 * Outputs animation string
 *
 * @param string $effect
 * @param int $delay
 * @param int $duration
 * @param int $vertOffset
 *
 * @return string
 */
function addAnimation($effect = 'fadeIn', $delay = 0, $duration = 0, $vertOffset = 0) {

	$animateStr  = 'data-animate="' . $effect . '"';
	$animateStr .= $delay != 0 ? ' data-animate-delay="' . $delay . '"' : '';
	$animateStr .= $duration != 0 ? ' data-animate-duration="' . $duration . '"' : '';
	$animateStr .= $vertOffset != 0 ? ' data-appear-top-offset=" ' . $vertOffset . '"' : '';

	echo $animateStr;
}



/**
 * Registers custom scripts used throughout the site's templates
 */
function registerAssets()
{
    wp_enqueue_style(
        'google-fonts',
        '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'
    );

    wp_enqueue_style(
        'production-css',
        BUILD_URI . ((defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) ? 'production.css' : 'production.min.css')
    );

    if (defined('GRUNTICON_ENABLED') && GRUNTICON_ENABLED) {
        wp_enqueue_script(
            'grunticon-js',
            BUILD_URI . 'icons/grunticon.loader.js',
            array(),
            false,
            false
        );
    }

    wp_deregister_script('jquery');
    wp_enqueue_script(
        'jquery',
        '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js',
        array(),
        false,
        true
    );

    wp_enqueue_script(
        'production-js',
        BUILD_URI . ((defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) ? 'production.js' : 'production.min.js'),
        array('jquery'),
        false,
        true
    );

    wp_localize_script(
        'production-js',
        'productionJS',
        array(
            'baseUrl'   => BASE_URL,
            'themeUrl'  => THEME_URI,
            'jsonUrl'   => JSON_URL,
            'ajaxUrl'   => AJAX_URL
        )
    );
}
add_action('wp_enqueue_scripts', 'registerAssets');


/**
 * Add grunt icon css urls
 */
function gruntIcon()
{
    if (defined('GRUNTICON_ENABLED') && GRUNTICON_ENABLED) {
        echo '
            <script>
                grunticon(["'.BUILD_URI .'/icons/icons.data.svg.css", "'.BUILD_URI .'/icons/icons.data.png.css", "'.BUILD_URI.'/icons/icons.fallback.css"], grunticon . svgLoadedCallback);
            </script>
        ';
    }
}
add_action('wp_head', 'gruntIcon');


/**
 * Add IE Conditional scripts for Modenizr and Respond.js
 */
function ieConditionalScripts()
{
    echo '
        <!--[if IE 8|IE 9]>
            <script src="' . BUILD_URI . 'js/' . ((defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) ? 'modernizr.js' : 'modernizr.min.js') . '"></script>
        <![endif]-->
        <!--[if IE 8]>
            <script src="' . BUILD_URI . 'js/respond.js"></script>
        <![endif]-->
    ';
}
add_action('wp_head', 'ieConditionalScripts');