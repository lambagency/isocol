<?php

function create_post_type() {
    	register_post_type('testmionials',
    	    array(
    
    	        'labels' => array(
    	            'name'                  => __('Testimonials'),
    	            'singular_name'         => __('Testimonial'),
    	            'add_new'               => __('Add New'),
    	            'add_new_item'          => __('Add New Testimonial'),
    	            'edit_item'             => __('Edit Testimonial'),
    	            'new_item'              => __('New Testimonial'),
    	            'all_items'             => __('All Testimonials'),
    	            'view_item'             => __('View Testimonial'),
    	            'search_items'          => __('Search Testimonials'),
    	            'not_found'             => __('No Testimonials found'),
    	            'not_found_in_trash'    => __('No Testimonials found in Trash'),
    	            'parent_item_colon'     => '',
    	            'menu_name'             => 'Testimonials'
    	        ),
    
    	        'publicly_queryable'    => true,
    	        'public'                => true,
    	        'show_ui'               => true,
    	        'hierarchical'          => false,
    	        'menu_position'         => null,
    	        'query_var'             => true,
    	        'rewrite'               => array('slug' => 'testmionials', 'with_front' => false),
    	        'supports'              => array('title', 'editor'),
    	        'taxonomies'            => array('post_tag'),
    	        'has_archive'           => false
    	    )
    	);
}
add_action( 'init', 'create_post_type' );