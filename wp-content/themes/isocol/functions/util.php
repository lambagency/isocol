<?php

namespace LambAgency\Util
{


    /**
     * Returns the common fields for the module in an array format.
     * Also allows additional fields to be retrieved.
     *
     *
     * @param array $additionalFields          - An associative array of additional fields to be retrieved.
     *                                         This array follows the structure $fieldName => $acfFieldKey
     *                                         (e.g. 'hideMobile => 'hide_mobile')
     *
     * @param array $additionalOptionsFields   - An associative array of additional options fields to be retrieved.
     *                                         This array follows the structure $fieldName => $acfFieldKey
     *                                         (e.g. 'hideMobile => 'hide_mobile')
     *
     * @return array - The array of fields
     */
    function getFields($additionalFields = array(), $additionalOptionsFields = array())
    {

        // Custom fields
        //------------------

        $fields = array();

        $fields['type'] = get_sub_field('type');
        $fields['styleType'] = get_sub_field('style_type');

        $fields['title'] = get_sub_field('title');
        $fields['subtitle'] = get_sub_field('subtitle');
        $fields['content'] = get_sub_field('content');

        $fields['bgColour'] = get_sub_field('background_colour');

        $fields['bgImage'] = get_sub_field('background_image');
        $fields['bgImageURL'] = $fields['bgImage'] ? $fields['bgImage']['sizes']['module-bg'] : '';
        $fields['bgImageMobileURL'] = $fields['bgImage'] ? $fields['bgImage']['sizes']['module-bg-mobile'] : '';

        $fields['buttons'] = array(
            'buttons' => get_sub_field('buttons')
        );

        // Loop through additional fields
        foreach ($additionalFields as $key => $additionalField) {

            // Ensure associative array
            if (is_string($key)) {
                $fields[(string)$key] = get_sub_field($additionalField);
            }
        }

        // Loop through additional options fields
        foreach ($additionalOptionsFields as $key => $additionalOptionsField) {

            // Ensure associative array
            if (is_string($key)) {
                $fields[(string)$key] = get_field($additionalOptionsField, 'option');
            }
        }


        // set default type
        if (!$fields['type']) {
            $fields['type'] = 'default';
        }


        // Module Class
        //------------------

        $fields['moduleClass'] = array();

        // add type to classes
        $fields['moduleClass'][] = $fields['type'];
        $fields['moduleClass'][] = $fields['styleType'];

        (isset($fields['bgColour']) && $fields['bgColour']) ? $fields['moduleClass'][] = 'bg-' . $fields['bgColour'] : false; // Background Colour
        (isset($fields['bgImage']) && $fields['bgImage']) ? $fields['moduleClass'][] = 'has-bg' : false; // Background Image
        (isset($fields['hideMobile']) && $fields['hideMobile']) ? ($fields['moduleClass'][] = 'hide-mobile') : false; // Hide Mobile


        return $fields;
    }

    /**
     * Get link set in theme settings by name
     *
     * @param $linkName - The link identifying name
     *
     * @return string - The link URL
     */
    function getLink($linkName)
    {
        $linkURL = '';

        if (isset($linkName) && $linkName != '') {

            $links = get_field('links', 'option');

            foreach ($links as $link) {

                if ($link['name'] == $linkName) {
                    $linkURL = $link['link'];

                    break;
                }
            }
        }

        return $linkURL;
    }


    /**
     * Generate anchor link for email address
     *
     * @param string $emailAddress       - The email address
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - The email address anchor html
     */
    function emailAnchor($emailAddress = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {
        $emailHTML = '<a href="mailto:' . antispambot($emailAddress) . '" target="_blank" class="email">';

        // Override Inner HTML
        if ($innerHTMLPlacement == 'override') {
            $emailHTML .= $innerHTML;
        }
        // Append Inner HTML
        else {
            if ($innerHTMLPlacement == 'append') {
                $emailHTML .= antispambot($emailAddress) . $innerHTML;
            }
            // Prepend Inner HTML
            else {
                $emailHTML .= $innerHTML . antispambot($emailAddress);
            }
        }

        $emailHTML .= '</a>';

        return $emailHTML;
    }


    /**
     * Echo generated email anchor
     *
     * @param string $emailAddress       - The email address
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - Echo out email address anchor html
     */
    function theEmailAnchor($emailAddress = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {

        $emailHTML = $emailAddress ? emailAnchor($emailAddress, $innerHTML, $innerHTMLPlacement) : '';

        echo $emailHTML;
    }


    /**
     * Generate anchor link for phone number
     *
     * @param string $phoneNumber        - The phone number
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - The phone number anchor html
     */
    function phoneAnchor($phoneNumber = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {

        $phoneLink = preg_replace('/[^0-9]/', '', $phoneNumber); // Remove all characters except digits

        $phoneHTML = '<a href="tel:' . $phoneLink . '" class="phone">';

        // Override Inner HTML
        if ($innerHTMLPlacement == 'override') {
            $phoneHTML .= $innerHTML;
        }
        // Append Inner HTML
        else {
            if ($innerHTMLPlacement == 'append') {
                $phoneHTML .= $phoneNumber . $innerHTML;
            }
            // Prepend Inner HTML
            else {
                $phoneHTML .= $innerHTML . $phoneNumber;
            }
        }

        $phoneHTML .= '</a>';

        return $phoneHTML;
    }


    /**
     * Echo generated phone number anchor
     *
     * @param string $phoneNumber        - The phone number
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - Echo out phone number anchor html
     */
    function thePhoneAnchor($phoneNumber = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {
        $phoneHTML = $phoneNumber ? phoneAnchor($phoneNumber, $innerHTML, $innerHTMLPlacement) : '';

        echo $phoneHTML;
    }


    /**
     * Generates the html for Grunticon
     *
     * @param           $iconName   - The name of the icon
     * @param bool|true $embed      - If true embed the SVG as html, otherwise use as background image
     *
     * @return string - The icon html
     */
    function grunticon($iconName, $embed = true) {

        $svgHTML = '';

        if ($iconName) {
            $iconClass = 'icon-'.$iconName;

            $svgHTML = '<div class="icon '.$iconClass.'"';
            $svgHTML .= $embed ? ' data-grunticon-embed>' : '>';
            $svgHTML .= '</div>';
        }

        return $svgHTML;
    }


    /**
     * Echo the generated Grunticon html
     *
     * @param           $iconName   - The name of the icon
     * @param bool|true $embed      - If true embed the SVG as html, otherwise use as background image
     *
     * @return string - The icon html
     */
    function theGrunticon($iconName = '', $embed = true) {

        $grunticonHTML = grunticon($iconName, $embed);

        echo $grunticonHTML;
    }
}