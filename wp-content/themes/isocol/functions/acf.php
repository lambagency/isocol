<?php


/**
 *  Hide ACF menu for dev/live environment
 */
function remove_acf_menu()
{
    if (!defined('DEV_ENVIRONMENT') || !DEV_ENVIRONMENT) {
        remove_menu_page('edit.php?post_type=acf-field-group');
    }
}

add_action('admin_menu', 'remove_acf_menu', 100);


/**
 * Create option pages
 */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug'  => 'theme-settings',
        'capability' => 'edit_posts',
        'redirect'   => false
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Module Settings',
        'menu_title'  => 'Module Settings',
        'slug'        => 'module-settings',
        'parent_slug' => 'theme-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Config',
        'menu_title'  => 'Config',
        'slug'        => 'config',
        'parent_slug' => 'theme-settings',
    ));
}


/**
 * Dynamically populates colour options for any field with the name 'colour'
 */
function colour_choices($field)
{

    $field['choices'] = array();

    if (have_rows('colours', 'option')) {

        while (have_rows('colours', 'option')) {

            the_row();

            $value = get_sub_field('value');
            $label = get_sub_field('label');

            $field['choices'][$value] = $label;
        }
    }

    return $field;
}

add_filter('acf/load_field/name=button_colour', 'colour_choices');


/**
 * Dynamically populates colour options for any field with the name 'background_colour'
 */
function background_colour_choices($field)
{

    $field['choices'] = array();

    if (have_rows('background_colours', 'option')) {

        while (have_rows('background_colours', 'option')) {

            the_row();

            $value = get_sub_field('value');
            $label = get_sub_field('label');

            $field['choices'][$value] = $label;
        }
    }

    return $field;
}

add_filter('acf/load_field/name=background_colour', 'background_colour_choices');


/**
 * Dynamically populates button type options for any field with the name 'button_type'
 */
function button_type_choices($field)
{
    $field['choices']['link']           = 'Link';
    $field['choices']['external']       = 'External Link';
    $field['choices']['download']       = 'File Download';
    $field['choices']['phone']          = 'Phone';
    $field['choices']['email']          = 'Email';

    return $field;
}

add_filter('acf/load_field/name=button_type', 'button_type_choices');


/**
 * Dynamically populates icon options for any field with the name 'button_icon'
 */
function button_icon_choices($field)
{

    $field['choices'] = array();

    if (have_rows('button_icons', 'option')) {

        while (have_rows('button_icons', 'option')) {

            the_row();

            $value = get_sub_field('value');
            $label = get_sub_field('label');

            $field['choices'][$value] = $label;
        }
    }

    if (empty($field['choices'])) {
        $field['choices']['fa-arrow-right'] = 'Arrow Right';
    }

    return $field;
}

add_filter('acf/load_field/name=button_icon', 'button_icon_choices');


/**
 * Dynamically populates button icon position options for any field with the name 'button_icon_position'
 */
function button_icon_position_choices($field)
{
    $field['choices']['left']      = 'Left';
    $field['choices']['far-left']  = 'Far Left';
    $field['choices']['right']     = 'Right';
    $field['choices']['far-right'] = 'Far Right';

    return $field;
}

add_filter('acf/load_field/name=button_icon_position', 'button_icon_position_choices');


/**
 *  Dynamically populates form options
 */
function form_type_choices($field)
{

    $field['choices'] = array();

    if (have_rows('forms', 'option')) {

        while (have_rows('forms', 'option')) {

            the_row();

            $value = get_sub_field('form_id');
            $label = ucwords(str_replace('-', ' ', $value));

            $field['choices'][$value] = $label;
        }
    }

    return $field;
}

add_filter('acf/load_field/name=form_type', 'form_type_choices');