<?php

namespace Lambagency\Ajax
{

    /**
     * Get job posts ajax action. Either echos JSON or returns response
     *
     * @param $args     - Array of post arguments
     * @param $return   - True to return response, false will echo response as JSON
     *
     * @return array    - Returns array containing jobs html, max pages, next_page
     */
    function testimonialPosts($args = array(), $searchFields = array(), $return = false)
    {
        $defaultArgs = array(
            'post_type' => 'testimonial'
        );

        $ajaxConfig = array(
            'templateURL'   => 'layouts/testimonial/testimonial-partial.php',
            'emptyMessage'  => 'Sorry, we could\'t find any testimonials.'
        );

        return ajaxPostsHTML($defaultArgs, $args, $searchFields, $ajaxConfig, $return);
    }


    /**
     * Get blog posts ajax action. Either echos JSON or returns response
     *
     * @param $args     - Array of post arguments
     * @param $return   - True to return response, false will echo response as JSON
     *
     * @return array    - Returns array containing blog posts html, max pages, next_page
     */
    function blogPosts($args = array(), $searchFields = array(), $return = false)
    {
        $defaultArgs = array(
            'post_type'         => 'post',
            'posts_per_page'    => get_option('posts_per_page') // Default set in wordpress admin
        );

        $ajaxConfig = array(
            'templateURL'   => 'layouts/blog/blog-partial.php',
            'emptyMessage'  => 'Sorry, we could\'t find any posts matching your search.'
        );

        return ajaxPostsHTML($defaultArgs, $args, $searchFields, $ajaxConfig, $return);
    }


    /**
     * @param array      $defaultArgs           - Array of default post arguments
     * @param array      $args                  - Array of optional post arguments
     * @param array      $searchFields          - Array of optional search fields, will be used to construct meta query array({name => value})
     * @param array      $ajaxConfig            - The config for the php get posts call, include template url and empty message.
     * @param bool|false $return                - True to return response, false will echo response as JSON
     *
     * @return array|string - Returns array containing blog posts html, max pages, next_page
     */
    function ajaxPostsHTML($defaultArgs = array(), $args = array(), $searchFields = array(), $ajaxConfig = array(), $return = false)
    {

        $defaultPostsPerPage = 6;

        // Arguments Setup
        //---------------------------------

        $postArgs = array(
            'post_status'       => 'publish',
            'paged'             => 1,
            'posts_per_page'    => $defaultPostsPerPage,
            'order'             => 'DESC'
        );

        if (is_array($defaultArgs)) {
            $postArgs = array_merge($postArgs, $defaultArgs); // Merge default post arguments with post arguments
        }

        if (is_array($args)) {
            $postArgs = array_merge($postArgs, $args); // Merge optional post arguments with post arguments
        }


        // POST Variables Setup
        //---------------------------------

        if (isset($_POST['page'])) {
            $postArgs['paged']  = intval($_POST['page']);
        }

        if (isset($_POST['posts_per_page'])) {
            $postArgs['posts_per_page'] = intval($_POST['posts_per_page']);
        }

        if (isset($_POST['order'])) {
            $postArgs['order'] = $_POST['order'];
        }

        if (isset($_POST['orderby'])) {
            $postArgs['orderby'] = $_POST['orderby'];
        }

        if (isset($_POST['cat'])) {
            $postArgs['cat'] = intval($_POST['cat']);
        }

        if (isset($_POST['search_fields'])) {
            $searchFields = $_POST['search_fields'];
        }


        // Custom Search Fields
        //---------------------------------

        if ($searchFields) {

            $metaQuery = array(
                'relation' => 'AND'
            );

            foreach ($searchFields as $searchField) {

                $metaQuery[] = array(
                    'key'     => $searchField['name'],
                    'value'   => $searchField['value'],
                    'compare' => '='
                );
            }

            $postArgs['meta_query'] = $metaQuery;
        }


        // Posts Query
        //---------------------------------

        $query = new \WP_Query($postArgs);
        $maxPages = $query->max_num_pages;
        $nextPage = intval($postArgs['paged']) + 1;

        ob_start();

        if ($query->have_posts()) {

            // Start index bases on page number, index will match post index
            $index = ($postArgs['paged'] - 1) * $postArgs['posts_per_page'];

            while ($query->have_posts()) {
                $query->the_post();

                if (isset($ajaxConfig['templateURL'])) {
                    include(locate_template($ajaxConfig['templateURL']));
                }
                $index++;
            }
        }
        else {
            if ($ajaxConfig['emptyMessage']) {
                echo '<div class="empty-results">' . $ajaxConfig['emptyMessage'] . '</div>';
            }
        }
        wp_reset_postdata();

        $response = ob_get_contents();
        ob_end_clean();


        $response = array(
            'nextPage'  => $nextPage,
            'maxPages'  => $maxPages,
            'html'      => $response
        );

        if ($return) {
            return $response; // Return response
        } else {
            echo json_encode($response); // Echo JSON response
        }

        exit;
    }
}

namespace
{

    // Testimonials Ajax
    add_action('wp_ajax_testimonialPosts', '\Lambagency\Ajax\testimonialPosts', 10, 2);
    add_action('wp_ajax_nopriv_testimonialPosts', '\Lambagency\Ajax\testimonialPosts', 10, 2);

    // Posts Ajax
    add_action('wp_ajax_blogPosts', '\Lambagency\Ajax\blogPosts', 10, 2);
    add_action('wp_ajax_nopriv_blogPosts', '\Lambagency\Ajax\blogPosts', 10, 2);
}

