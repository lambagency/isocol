<?php

/**
 * Custom error logging function
 */
if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }
    }
}


/**
 * Simply function to print formatted variable to screen
 *
 * @param $data
 */
function dump($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}