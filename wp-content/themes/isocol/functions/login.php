<?php


/**
 * Update login to use email address instead of username
 */
remove_filter('authenticate', 'wp_authenticate_username_password', 20);
add_filter('authenticate', function ($user, $email, $password) {

    // Check for empty fields
    if (empty($email) || empty($password)) {

        // create new error object and add errors to it.
        $error = new WP_Error();

        if (empty($email)) {
            $error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.'));
        } else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.'));
            }
        }

        if (empty($password)) {
            $error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.'));
        }

        return $error;
    }

    // Check if user exists in WordPress database
    $user = get_user_by('email', $email);

    if (!$user) {
        $error = new WP_Error();
        $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));

        return $error;
    } else {

        if (!wp_check_password($password, $user->user_pass, $user->ID)) {
            $error = new WP_Error();
            $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));

            return $error;
        } else {
            return $user;
        }
    }
}, 20, 3);


/**
 * Update lables on login page
 */
function login_function()
{
    add_filter('gettext', 'username_change', 20, 3);

    function username_change($translated_text, $text, $domain)
    {
        if ($text === 'Username') {
            $translated_text = 'Email';
        }

        return $translated_text;
    }
}
add_action('login_head', 'login_function');



/**
 * Replace WP logo on login screen with NS logo
 */
function custom_loginlogo()
{
    if (function_exists('get_field')) {
        $logo = get_field('header_logo', 'option');
        $logoUrl = $logo ? $logo['sizes']['header-logo'] : '';
        $logoWidth = $logo ? $logo['sizes']['header-logo-width'] : '';
        $logoHeight = $logo ? $logo['sizes']['header-logo-height'] : '';

        $primaryColour = '#477dca';

        echo '<style type="text/css">

           ' . ($logo ? '.login h1 a { width: ' . $logoWidth . 'px; height: ' . $logoHeight . 'px; background-image: url(' . $logoUrl . '); background-size: cover; }' : '') . '

            .login #backtoblog a:hover, .login #nav a:hover { color: ' . $primaryColour . '; }
            .wp-core-ui .button-primary, .wp-core-ui .button-primary:hover, .wp-core-ui .button-primary:focus { background: ' . $primaryColour . '; border-color: ' . $primaryColour . '; box-shadow: none !important; text-shadow: none !important; }
            .login label { color: #000; }
            .login .message { border-left-color: ' . $primaryColour . '; color: #000; }
            .login form { color: #000; }
        </style>';
    }
}
add_action('login_head', 'custom_loginlogo');


/**
 * Change WP URL on login page to home page
 *
 * @param $url
 *
 * @return string
 */
function custom_loginlogo_url($url)
{
    return BASE_URL;
}
add_filter('login_headerurl', 'custom_loginlogo_url');


/**
 * Hide admin bar for non-admin users
 */
function removeAdminBar()
{
    $user = wp_get_current_user();
    if (!in_array('administrator', $user->roles)) {
        show_admin_bar(false);
    }
}
//add_filter('after_setup_theme', 'removeAdminBar');


/**
 * Sets redirect to home page after registration
 *
 * @return string
 */
function registrationRedirect()
{
    return BASE_URL;
}
//add_filter('registration_redirect', 'registrationRedirect');


/**
 * Remove access to admin unless administrator
 */
function dashboardAccessRestriction()
{
    $user = wp_get_current_user();
    if (!in_array('administrator', $user->roles) && !defined('DOING_AJAX')) {
        wp_redirect(BASE_URL);
        exit;
    }
}
//add_action('admin_init', 'dashboardAccessRestriction');


/**
 * Adds a unique token to the user's meta.
 *
 * @param $user_id
 */
function newRegistrationToken($user_id)
{
    add_user_meta($user_id, 'token', uniqid('', true), false);
}
//add_action('user_register', 'newRegistrationToken', 10, 1);