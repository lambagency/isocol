<?php

// General
//---------------------------------

define('GRUNTICON_ENABLED', false);
define('AUTOCOMPLETE_ENABLED', true);


// URLs
//---------------------------------

define('THEME_URI', get_stylesheet_directory_uri() );
define('BUILD_URI', THEME_URI . '/interface/build/');
define('THEME_JS', BUILD_URI . 'js');
define('THEME_IMAGES', THEME_URI . '/interface/img/');

define('BASE_URL', esc_url(home_url('/')));
define('JSON_URL', BASE_URL . 'wp-json/');
define('AJAX_URL', BASE_URL . 'wp-admin/admin-ajax.php');


// Email
//---------------------------------

define('ERROR_EMAIL', 'error-notifications@lambagency.com.au');