<?php

function project_init()
{
    remove_post_type_support('page', 'editor');
}
add_action('init', 'project_init');


function setup()
{
    // Add RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');


    // Enable support for Post Thumbnails
    add_theme_support('post-thumbnails');
    add_image_size('header-logo', 300, 150, false);
    add_image_size('footer-logo', 335, 115, false);
    add_image_size('footer-product', 120, 315, false);
    add_image_size('product-card', 400, 400 , false);
    add_image_size('product-collection', 250, 250, false);

    add_image_size('banner-bg', 1800, 600, false);
    add_image_size('banner-bg-mobile', 1000, 600, false);

    add_image_size('module-bg', 1800, 600, false);
    add_image_size('module-bg-mobile', 1000, 600, false);

    add_image_size('square-small', 200, 200, false);
    add_image_size('square-medium', 600, 600, false);
}
add_action('after_setup_theme', 'setup');



/** change WP default jpeg compression from 90 to 80 **/
add_filter('jpeg_quality', create_function('', 'return 80;'));



/**
 * Defines Widget areas
 */
function default_widgets_init()
{
    register_sidebar(array(
        'name'          => __('Sidebar Blog Post'),
        'id'            => 'sidebar-blog-post-widget',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget'  => "</li>",
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

    register_sidebar(array(
        'name'          => __('Sidebar Blog List', 'default'),
        'id'            => 'sidebar-blog-list-widget',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget'  => "</li>",
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));
}
add_action('widgets_init', 'default_widgets_init');


/**
 * Adds Page Slug Body Class
 *
 * @param $classes
 *
 * @return array
 */
function add_slug_body_class($classes)
{
    global $post;
    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }

    return $classes;
}
add_filter('body_class', 'add_slug_body_class');