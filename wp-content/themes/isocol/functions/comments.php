<?php

/**
 * Custom styling of comment template
 *
 * @param $comment
 * @param $args
 * @param $depth
 */
function customComment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);
	?>

<li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">

	<?php if ( $args['avatar_size'] != 0 ) : ?>
		<div class="avatar">
			<div class="img">
				<?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
			</div>
		</div>
	<?php endif; ?>


	<div class="detail">
		<div class="author">
			<?php echo get_comment_author_link(); ?>
		</div>

		<div class="date">
			<?php printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?>
		</div>

		<?php comment_text(); ?>
	</div>
	<?php
}