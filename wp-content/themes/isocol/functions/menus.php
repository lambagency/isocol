<?php


function registerMenus()
{
    register_nav_menus(array(
        'top'           => __('Top - Navigation', 'top-nav'),
        'header'        => __('Header - Navigation', 'header-nav'),
        'footer'        => __('Footer - Navigation', 'footer-nav'),
        'footer-legals' => __('Footer - Legals', 'footer-legals'),
    ));
}
add_action('after_setup_theme', 'registerMenus');


class Custom_Walker_Nav_Menu extends Walker_Nav_Menu {


    public function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<span class='sub-menu-toggle'></span><ul class='sub-menu'>\n";
    }


    public function end_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat('\t', $depth);
        $output .= "$indent</ul>\n";
    }


//    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
//        parent::start_el($output, $item, $depth = 0, $args = array(), $id = 0);
//    }
}
