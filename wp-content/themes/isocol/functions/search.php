<?php

namespace LambAgency;

/**
 * Join posts and postmeta tables
 *
 * @param $join
 *
 * @return string
 */
function searchJoin($join)
{
    global $wpdb;

    if (is_search()) {
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', '\LambAgency\searchJoin' );


/**
 * Allows multiple keywords to be search independently and also searches custom fields
 *
 * @param $where
 *
 * @return string
 */
function searchWhere($where)
{
    if (is_search()) {

        global $wpdb;

        if (array_key_exists('s', $_GET) && !array_key_exists('searchComplete', $_GET)) {
            $search = $_GET['s'];

            if (strpos($search, ' ') !== false) {
                $searchArray = explode(' ', $search);

                $startWhere = strpos($where, ')))  ');
                $newWhere = substr($where, $startWhere);

                $updatedWhere = 'AND (';

                foreach ($searchArray as $keyword) {
                    $updatedWhere .= "(({$wpdb->posts}.post_title LIKE '%{$keyword}%') OR (lamb_wp_posts.post_content LIKE '%{$keyword}%') OR ({$wpdb->postmeta}.meta_value LIKE '%{$keyword}%')) OR ";
                }

                $updatedWhere = substr($updatedWhere, 0, strlen($updatedWhere) - 6);

                $where = $updatedWhere . $newWhere;
            }
            else {
                $where = preg_replace(
                    "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                    "(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)", $where);
            }

            $_GET['searchComplete'] = true;
        }
    }

    return $where;
}
add_filter('posts_where' , '\LambAgency\searchWhere');


/**
 * Prevents duplicates from appearing in search results
 *
 * @param $where
 *
 * @return string
 */
function searchDistinct($where)
{
    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter('posts_distinct', '\LambAgency\searchDistinct' );


/**
 * Update number of search results to show per page
 *
 * @param $query
 *
 * @return mixed
 */
function searchPostsPerPage($query)
{
    if ($query->is_search) {
        $query->query_vars['posts_per_page'] = -1;
    }

    return $query;
}
add_filter('pre_get_posts', '\LambAgency\searchPostsPerPage');


/**
 * Creates a js cache file of all of the specified post types in a json array
 * Enqueues the script
 *
 * @param bool $forceRecache
 */
function searchAutocomplete($forceRecache = false)
{
    if (AUTOCOMPLETE_ENABLED) {

        $postTypes  = array('post', 'page');
        $filename   = 'autocomplete.json';
        $absFile    = __DIR__ . '/' . $filename;
        $file       = get_stylesheet_directory_uri() . '/functions/' . $filename;


        // default cache time set to 1 hour
        $cacheTime = 1 * 60 * 60;

        // time that the cache file was last updated.
        $cacheFileCreated  = ((file_exists($absFile))) ? filemtime($absFile) : 0;
        $meetsCacheThreshold = (time() - $cacheTime) > $cacheFileCreated;

        // check if file needs to be re-cached
        if ($meetsCacheThreshold || $forceRecache) {

            $results = array();

            $query = new \WP_Query(array(
                'post_type'         => $postTypes,
                'post_status'       => 'publish',
                'posts_per_page'    => -1,
                'orderby'           => 'type',
                'order'             => 'ASC'
            ));

            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();

                    $title  = get_the_title();
                    $type   = get_post_type();
                    $url    = get_the_permalink();

                    $results[] = array(
                        'title' => $title,
                        'type'  => $type,
                        'url'   => esc_url($url)
                    );
                }
            }

            $jsOutput = json_encode($results);

            // write to file
            file_put_contents($absFile, $jsOutput);
        }

        wp_enqueue_script(
            'autocomplete-js',
            $file,
            array('jquery'),
            false,
            true
        );

        wp_localize_script(
            'autocomplete-js',
            'autocompleteJS',
            array(
                'url'           => $file,
                'searchLink'    => BASE_URL . 'search/'
            )
        );
    }
}
add_action('wp_enqueue_scripts', '\LambAgency\searchAutocomplete');


/**
 * Forces autocomplete.json to be recached
 */
function updateAutocompleteData()
{
    \LambAgency\searchAutocomplete(true);
}
add_action('save_post', '\LambAgency\updateAutocompleteData');