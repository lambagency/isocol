<?php


/**
 * Forces Gravity Forms inline scripts to footer (after jQuery has been initialised)
 */
function init_scripts()
{
    return true;
}

add_filter('gform_init_scripts_footer', 'init_scripts');


/**
 * Gravity Wiz // Disable HTML5 Validation on Gravity Forms
 */
function add_no_validate_attribute_to_form_tag($form_tag)
{
    return str_replace('>', ' novalidate="novalidate">', $form_tag);
}

add_filter('gform_form_tag', 'add_no_validate_attribute_to_form_tag');


/**
 * Adjusting the HTML of the submit and next buttons to match design
 */
function style_submit_button($button, $form)
{
    return '<button type="submit" id="gform_submit_button_' . $form["id"] . '" role="button" class="btn-primary">' . $form["button"]["text"] . '</button>';
}

add_filter('gform_submit_button', 'style_submit_button', 10, 2);


function style_next_button($button, $form)
{
    return '<button id="gform_submit_button_' . $form["id"] . '" role="button" class="btn-next btn-primary">Next Step</button>';
}

add_filter('gform_next_button', 'style_next_button', 10, 2);


function style_previous_button($button, $form)
{
    return '<button role="button" class="btn-previous btn-primary">Previous Step</button>';
}

add_filter('gform_previous_button', 'style_previous_button', 10, 2);


/**
 * Specify custom ajax spinner image
 */
function spinner_url($image_src, $form)
{
    //	return THEME_IMAGES . 'icons/ajax-loader.gif';
    return '';
}

//add_filter('gform_ajax_spinner_url', 'spinner_url', 10, 2);


/**
 * Disables the automatic scroll to the confirmation anchor
 */
//add_filter('gform_confirmation_anchor', create_function('', 'return false;'));


/**
 * Dynamically create a username when using the user registration
 * add on with Gravity Forms
 */
function auto_username($username, $config, $form, $entry)
{
    $username = strtolower($entry['2.3'] . $entry['2.6']);

    if (empty($username)) {
        return $username;
    }

    if (!function_exists('username_exists')) {
        require_once(ABSPATH . WPINC . "/registration.php");
    }

    if (username_exists($username)) {
        $i = 2;

        while (username_exists($username . $i)) {
            $i++;
        }

        $username = $username . $i;
    };

    return $username;
}

//add_filter('gform_username', 'auto_username', 10, 4);


/**
 * Add australian states to the GForms address
 *
 * @return mixed
 */
function get_au_states()
{
    return apply_filters(
        'gform_au_states', array(

            esc_html__('Australian Capital Territory', 'gravityforms'),
            esc_html__('New South Wales', 'gravityforms'),
            esc_html__('Northern Territory', 'gravityforms'),
            esc_html__('Queensland', 'gravityforms'),
            esc_html__('Southern Australia', 'gravityforms'),
            esc_html__('Victoria', 'gravityforms'),
            esc_html__('Tasmania', 'gravityforms'),
            esc_html__('Western Australia', 'gravityforms'),

        )
    );
}


add_filter('gform_address_types', function ($addressTypes, $form_id) {

    $addressTypes['au'] = array(
        'label'       => esc_html__('Australia', 'gravityforms'),
        'zip_label'   => gf_apply_filters('gform_address_zip', $form_id, esc_html__('Postcode', 'gravityforms'), $form_id),
        'state_label' => gf_apply_filters('gform_address_state', $form_id, esc_html__('State', 'gravityforms'), $form_id),
        'country'     => 'Australia',
        'states'      => array_merge(array(''), get_au_states())
    );

    return $addressTypes;
}, 15, 2);



/**
 * Update all notification emails before sending.
 * If live site, add backup@lambagency.com.au as BCC recipient
 *
 * @param $notification
 * @param $form
 * @param $entry
 *
 * @return mixed
 */
function updateNotificationEmails($notification, $form, $entry)
{
    $notification['subject'] = (isset($notification['fromName']) ? $notification['fromName'] . ' - ' : '') . $notification['subject'];

    if (defined('DEV_ENVIRONMENT') && !DEV_ENVIRONMENT) {
        $notification['bcc'] = 'archive@lambagency.com.au';
    }

    return $notification;
}

add_filter('gform_notification', 'updateNotificationEmails', 10, 3);