
module.exports = function(grunt){

    "use strict";
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);


    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


        clean: {
            build: {
                src: "<%= templateDir %>interface/build/"
            },
            grunticon: {
                src: "<%= templateDir %>interface/build/icons"
            }
        },


        copy: {
            main: {
                files: [

                    // CORE JS
                    {
                        expand: false,
                        src: 'bower_components/jquery/dist/jquery.min.js',
                        dest: '<%= templateDir %>interface/build/js/jquery.min.js'
                    },

                    {
                        expand: false,
                        src: 'bower_components/modernizr/modernizr.js',
                        dest: '<%= templateDir %>interface/build/js/modernizr.js'
                    },

                    {
                        expand: false,
                        src: 'bower_components/respond/dest/respond.min.js',
                        dest: '<%= templateDir %>interface/build/js/respond.js'
                    }
                ]
            }
        },


        sass: {
            interface: {
                files : {
                    '<%= templateDir %>interface/build/production.css': '<%= templateDir %>interface/scss/main.scss'
                }
            },
            layouts: {
                files : {
                    '<%= templateDir %>interface/build/layouts/layouts.css': '<%= templateDir %>layouts/layouts.scss'
                }
            },
            plugins: {
                files : {
                    '<%= templateDir %>interface/build/scss-plugins/easy-autocomplete.css': 'bower_components/EasyAutocomplete/src/sass/easy-autocomplete.scss'
                }
            }
        },


        concat: {
            js : {
                src: [
                    'bower_components/slick.js/slick/slick.min.js',
                    'bower_components/matchHeight/jquery.matchHeight-min.js',
                    'bower_components/blazy/blazy.min.js',
                    'bower_components/jquery-validation/dist/jquery.validate.min.js',
                    'bower_components/EasyAutocomplete/dist/jquery.easy-autocomplete.js',

                    '<%= templateDir %>interface/js/plugins/*.js',
                    '<%= templateDir %>interface/js/custom-plugins/*.js',
                    '<%= templateDir %>interface/js/*.js',
                    '<%= templateDir %>layouts/**/*.js',
                    '<%= templateDir %>layouts/*.js'
                ],
                dest: '<%= templateDir %>interface/build/production.js'
            },

            css : {
                src: [
                    '<%= pkg.templateDir %>interface/build/production.css',
                    '<%= pkg.templateDir %>interface/build/layouts/layouts.css'
                ],
                dest: '<%= pkg.templateDir %>interface/build/production.css'
            }
        },


        uglify: {
            build: {
                files: {
                    '<%= templateDir %>interface/build/production.min.js': ['<%= templateDir %>interface/build/production.js'],
                    '<%= templateDir %>interface/build/js/modernizr.min.js': ['<%= templateDir %>interface/build/js/modernizr.js'],
                    '<%= templateDir %>interface/build/js/respond.min.js': ['<%= templateDir %>interface/build/js/respond.js']
                }
            }
        },


        cssmin: {
            options: {
                compatibility: 'ie8',
                shorthandCompacting: false
            },
            build: {
                src: ['<%= templateDir %>interface/build/production.css'],
                dest: '<%= templateDir %>interface/build/production.min.css'
            }
        },


        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= templateDir %>interface/grunticon',
                    src: ['*.svg'],
                    dest: '<%= templateDir %>interface/build/icons/svg'
                }],
                options: {
                    // For all plugins: https://github.com/svg/svgo/tree/master/plugins
                    plugins: [
                        //{mergePaths: false}
                    ]
                }
            }
        },

        grunticon: {
            myIcons: {
                files: [{
                    expand: true,
                    //cwd: '<%= templateDir %>interface/build/icons/svg',
                    cwd: '<%= templateDir %>interface/',
                    src: ['build/icons/svg/*.svg', 'grunticon/*.png'],
                    dest: '<%= templateDir %>interface/build/icons'
                }],
                options: {
                    enhanceSVG: true,
                    colors: {

                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: '<%= templateDir %>interface/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: '<%= templateDir %>interface/img/'
                }]
            },
            uploads: {
                files: [{
                    expand: true,
                    cwd: '<%= templateDir %>../../uploads',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: '<%= templateDir %>../../uploads'
                }]
            }
        }
    });



    grunt.registerTask('set_global', 'Set a global variable.', function(name, val) {
        global[name] = val;
    });

    grunt.registerTask('set_config', 'Set a config property.', function(name, val) {
        grunt.config.set(name, val);
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-grunticon');


    grunt.registerTask('default', [
        'set_config:templateDir:<%= pkg.templateDir %>',
        'clean',
        'copy',
        'sass',
        'concat'
    ]);


    grunt.registerTask('production', [
        'clean:build',
        'default',
        'uglify',
        'cssmin'
    ]);


    grunt.registerTask('production-images', [
        'production',
        'imagemin'
    ]);


    grunt.registerTask('icons', [
        'set_config:templateDir:<%= pkg.templateDir %>',
        'clean:grunticon',
        'svgmin',
        'grunticon'
    ]);

};